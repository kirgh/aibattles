﻿using System;
using System.Linq;
using Z.KM.AIBattles.Common.AI;
using Z.KM.AIBattles.Common.Geometry;

namespace Z.KM.AIBattles.TestAi
{
    public class TestAiFactory : IAiFactory
    {
        public IBattleUnitAi CreateUnitAi(CreateUnitAiData data)
        {
            return new TestAiFactoryUnitAi();
        }

        public void Init(InitAiFactoryData data)
        {
            //nothing
        }

        private class TestAiFactoryUnitAi : IBattleUnitAi
        {
            public UpdateUnitResult Update(UpdateUnitAiData data)
            {
                UpdateUnitResult result;

                var firstEnemy = data.Perception.VisibleUnits
                    .Where(unit => !unit.Friendly)
                    .FirstOrDefault();
                if (firstEnemy != null && data.Perception.Energy >= data.Perception.ThrowEnergyCost)
                {
                    UpdateUnitResult.AttackDesire attack;
                    attack.Enabled = true;
                    attack.Direction = firstEnemy.Direction;
                    result.Attack = attack;
                }
                else
                {
                    result.Attack = default(UpdateUnitResult.AttackDesire);
                }
                result.Move.Enabled = false;
                result.Move.Direction = Vector2D.Zero;
                return result;
            }
        }
    }
}
