﻿using KM.Log;
using System;
using System.Collections.Generic;
using Z.KM.AIBattles.Common.Geometry;

namespace Z.KM.AIBattles.Development
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Logger.SetImplementation(DebugAndConsoleLoggerImplementation.Instance);
                TestVector2D();
                AIBattlesTest.Test();
            } while (Console.ReadKey().Key != ConsoleKey.Spacebar);
        }

        private static void TestVector2D()
        {
            var vectors = new List<Vector2D>
                {
                    new Vector2D(1, 0),
                    new Vector2D(-15, 0),
                    new Vector2D(0, 1),
                    new Vector2D(0, 15),
                    new Vector2D(7, 3),
                    new Vector2D(-5, 5),
                };

            var basies = new List<Vector2D>
                {
                    new Vector2D(0, 1),
                    new Vector2D(1, 0),
                    new Vector2D(0.7f, 0.7f),
                };

            foreach (var basis in basies)
            {
                Logger.Log("basis = " + basis.ToString());
                foreach (var vector in vectors)
                {
                    var rotated = vector.ToAnotherSystem(basis);
                    var rotatedTwice = rotated.FromAnotherSystem(basis);
                    Logger.Log(vector.ToString() + " => " + rotated.ToString() + " => " + rotatedTwice);
                }
            }
        }
    }
}
