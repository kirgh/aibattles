﻿using KM.Log;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Z.KM.AIBattles.Common.AI;
using Z.KM.AIBattles.Common.Core;
using Z.KM.AIBattles.Common.Geometry;
using Z.KM.AIBattles.Core;

namespace Z.KM.AIBattles.Development
{
    public class AIBattlesTest
    {
        public static void Test()
        {
            Logger.Log("AIBattlesTest.Test.Enter");
            CreateBattleSettings settings;

            var terrainPosition = new Common.Geometry.Vector2D(150, 250);
            var terrainRadius = 15;
            var teamSize = 4;

            CreateBattleSettings.Team teamA;
            teamA.AI = new TestAiFactory();
            teamA.Id = 0;
            teamA.Units = CreateUnits(teamSize, terrainPosition, terrainRadius, false);

            CreateBattleSettings.Team teamB;
            teamB.AI = new TestAiFactory();
            teamB.Id = 1;
            teamB.Units = CreateUnits(teamSize, terrainPosition, terrainRadius, true);

            var teams = new List<CreateBattleSettings.Team>{
                teamA ,
                teamB
            };

            settings.Teams = teams.AsReadOnly();
            settings.ProjectileDamage = 10;
            settings.MaxBattleTime = 10;
            settings.StartShrinkTerrainTime = settings.MaxBattleTime * 0.5f;
            settings.TickInterval = 1 / 60f;
            settings.UnitRadius = 0.3f;
            settings.UnitsMaxSpeed = 5;
            settings.Energy.MaxEnergy = 10;
            settings.Energy.IncreaseSpeed = 1;
            settings.Energy.MoveDecreaseSpeed = 1;
            settings.Energy.AttackCost = 5;
            settings.ProjectileSpeed = 20;
            settings.ProjectileLifeTime = 3;
            settings.UnitDeceleration = 40;
            settings.UnitAcceleration = 40;
            settings.ProjectilePushTargetCoefficient = 0.2f;
            settings.TerrainRadius = terrainRadius;
            settings.TerrainPosition = terrainPosition;
            settings.RandomSeed = 0;
            var battle = AIBattlesDefaultFactory.CreateBattle(settings);
            Logger.Log("Initial state:");
            Logger.Log(BattleToString(battle));
            while (!battle.IsFinished())
            {
                battle.NextStep();
            }
            Logger.Log("Final state:");
            Logger.Log(BattleToString(battle));
            Logger.Log("AIBattlesTest.Test.Exit");
        }

        private static ReadOnlyCollection<CreateBattleSettings.Unit> CreateUnits(int teamSize, Vector2D terrainPosition, float terrainRadius, bool right)
        {
            var units = new List<CreateBattleSettings.Unit>();
            for (var i = 0; i < teamSize; i++)
            {
                CreateBattleSettings.Unit unit;
                unit.MaxHealth = 100;
                unit.Position = terrainPosition + new Vector2D(terrainRadius / 2, 0) * (right ? 1 : -1);
                units.Add(unit);
            }
            return units.AsReadOnly();
        }

        private static string BattleToString(IBattle battle)
        {
            Logger.TodoLowPriority("BattleRepresentation is bad class!");
            var result = new StringBuilder();
            var representation = battle.GetRepresentation();
            result.Append("Time: ").Append(representation.SecondsFromStart).AppendLine();
            result.AppendLine("Units:");
            foreach (var unit in representation.GetUnits())
            {
                result.Append(unit.GetDebugString()).AppendLine();
            }
            return result.ToString();
        }

        private class TestAiFactory : IAiFactory
        {
            public IBattleUnitAi CreateUnitAi(CreateUnitAiData data)
            {
                return new UnitAi();
            }

            public void Init(InitAiFactoryData data)
            {
                //nothing
            }

            private class UnitAi : IBattleUnitAi
            {
                public bool VisbileUnit { get; private set; }

                public UpdateUnitResult Update(UpdateUnitAiData data)
                {
                    UpdateUnitResult result;

                    var firstEnemy = data.Perception.VisibleUnits
                        .Where(unit => !unit.Friendly)
                        .FirstOrDefault();
                    if (firstEnemy != null && data.Perception.Energy >= data.Perception.ThrowEnergyCost)
                    {
                        UpdateUnitResult.AttackDesire attack;
                        attack.Enabled = true;
                        attack.Direction = firstEnemy.Direction;
                        result.Attack = attack;
                    }
                    else
                    {
                        result.Attack = default(UpdateUnitResult.AttackDesire);
                    }


                    if (firstEnemy != null)
                    {
                        UpdateUnitResult.MoveDesire Move;
                        Move.Enabled = true;
                        Move.Direction = firstEnemy.Direction;
                        result.Move = Move;
                    }
                    else
                    {
                        result.Move = default(UpdateUnitResult.MoveDesire);
                    }


                    return result;
                }
            }
        }
    }
}
