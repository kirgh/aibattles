﻿using Z.KM.AIBattles.Common.Geometry;

namespace Z.KM.AIBattles.Common.AI
{
    public struct UpdateUnitResult
    {
        public struct MoveDesire
        {
            public bool Enabled;
            /// <summary>
            /// 1 => MaxSpeed
            /// </summary>
            public Vector2D Direction;
        }

        public struct AttackDesire
        {
            public bool Enabled;
            public Vector2D Direction;
        }

        public MoveDesire Move;
        public AttackDesire Attack;
    }
}
