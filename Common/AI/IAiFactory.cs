﻿using System;

namespace Z.KM.AIBattles.Common.AI
{
    public interface IAiFactory
    {
        /// <summary>
        /// гарантированно вызовется до CreateUnitAi 
        /// </summary>
        void Init(InitAiFactoryData data);

        IBattleUnitAi CreateUnitAi(CreateUnitAiData data);
    }
}
