﻿using System;

namespace Z.KM.AIBattles.Common.AI
{
    public struct InitAiFactoryData
    {
        public Random Random;
        public int TeamId;
    }
}
