﻿using System.Collections.ObjectModel;
using Z.KM.AIBattles.Common.Geometry;

namespace Z.KM.AIBattles.Common.AI
{

    public enum HealthPerception
    {
        Critical,
        Low,
        Normal,
        Good,
        Healthy
    }

    public interface IPerceptionVisibleUnit
    {
        bool Friendly { get; }

        /// <summary>
        /// положение юнита относительно координатной системы наблюдателя (где ось Y - куда смотрит наблюдатель)
        /// </summary>
        Vector2D Direction { get; }

        float Radius { get; }

        /// <summary>
        /// скорость юнита (м/с) относительно координатной системы наблюдателя
        /// </summary>
        Vector2D Speed { get; }

        HealthPerception Health { get; }
        int TeamId { get; }
        int UniqueIdInTeam { get; }
    }

    public interface IPerceptionVisibleProjectile
    {
        /// <summary>
        /// положение снаряда относительно координатной системы наблюдателя
        /// </summary>
        Vector2D Direction { get; }

        /// <summary>
        /// скорость снаряда (м/с) относительно координатной системы наблюдателя
        /// </summary>
        Vector2D Speed { get; }
    }

    public struct UnitPerception
    {
        /// <summary>
        /// видимые юниты.
        /// на текущий момент это все живые юниты, свои и чужие, кроме наблюдателя.
        /// может быть изменено, если введём угол обзора.
        /// </summary>
        public ReadOnlyCollection<IPerceptionVisibleUnit> VisibleUnits;

        /// <summary>
        /// видимые снаряды.
        /// на текущий момент это все снаряды.
        /// может быть изменено, если введём угол обзора.
        /// </summary>
        public ReadOnlyCollection<IPerceptionVisibleProjectile> VisibleProjectiles;

        public HealthPerception Health;
        public float Radius;
        public float Energy;
        public float MaxEnergy;
        public float ThrowEnergyCost;

        /// <summary>
        /// скорость юнита относительно координатной системы наблюдателя
        /// (скорее всего, сейчас X всегда 0, но это временно, завязываться на это не стоит)
        /// </summary>
        public Vector2D CurrentSpeed;

        /// <summary>
        /// вектор до ближайшей точки круга территории, за которую нельзя выходить
        /// опять же относительно наблюдателя
        /// </summary>
        public Vector2D VectorToNearestTerrainEdge;
        public float TerrainRadius;
        
        /// <summary>
        /// признак, что территория уже начала сужаться
        /// </summary>
        public bool IsTerrainShrinking;
        public float TimeFromStart;
    }
}
