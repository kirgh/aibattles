﻿namespace Z.KM.AIBattles.Common.AI
{
    public struct CreateUnitAiData
    {
        /// <summary>
        /// уникальный id юнита в пределах одной команды
        /// </summary>
        public int UniqueIdInTeam;

        /// <summary>
        /// скорость прожектайлов (метров в секунду)
        /// </summary>
        public float ProjectileSpeed;

        /// <summary>
        /// скорость юнитов (метров в секунду)
        /// </summary>
        public float UnitSpeed;
    }
}
