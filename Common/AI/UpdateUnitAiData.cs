﻿namespace Z.KM.AIBattles.Common.AI
{
    public struct UpdateUnitAiData
    {
        public UnitPerception Perception;
        public int UniqueIdInTeam;
    }
}
