﻿namespace Z.KM.AIBattles.Common.AI
{
    public interface IBattleUnitAi
    {
        UpdateUnitResult Update(UpdateUnitAiData data);
    }
}
