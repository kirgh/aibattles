﻿using System.Collections.ObjectModel;
using Z.KM.AIBattles.Common.AI;
using Z.KM.AIBattles.Common.Geometry;

namespace Z.KM.AIBattles.Common.Core
{
    public struct CreateBattleSettings
    {
        public struct Team
        {
            public IAiFactory AI;
            public int Id;

            public ReadOnlyCollection<Unit> Units;
        }

        public struct Unit
        {
            public Vector2D Position;
            public int MaxHealth;
        }

        public struct UnitEnergySetttings
        {
            public float MaxEnergy;
            public float IncreaseSpeed;
            public float MoveDecreaseSpeed;
            public float AttackCost;
        }

        public ReadOnlyCollection<Team> Teams;
        public float ProjectileSpeed;
        public int ProjectileDamage;
        public float MaxBattleTime;
        public float StartShrinkTerrainTime;
        public float TickInterval;
        public float UnitRadius;
        public float UnitsMaxSpeed;
        public UnitEnergySetttings Energy;
        public float ProjectileLifeTime;
        public float UnitDeceleration;
        public float UnitAcceleration;
        public float ProjectilePushTargetCoefficient;
        public float TerrainRadius;
        public Vector2D TerrainPosition;
        public int RandomSeed;
    }
}
