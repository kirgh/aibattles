﻿using Z.KM.AIBattles.Common.Core.BattleRepresentation;

namespace Z.KM.AIBattles.Common.Core
{
    public interface IBattle
    {
        bool IsFinished();
        void NextStep();
        IBattleRepresentation GetRepresentation();
    }
}
