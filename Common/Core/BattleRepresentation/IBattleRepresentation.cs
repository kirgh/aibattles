﻿using System.Collections.ObjectModel;
using System.Text;
using Z.KM.AIBattles.Common.Geometry;

namespace Z.KM.AIBattles.Common.Core.BattleRepresentation
{
    public interface IBattleRepresentation
    {
        float SecondsFromStart { get; }
        float MaxBattleTime { get; }

        ReadOnlyCollection<IBattleUnitRepresentation> GetUnits();
        ReadOnlyCollection<IBattleProjectileRepresentation> GetProjectiles();
        ITerrain GetTerrain();
    }

    public interface IBattleUnitRepresentation
    {
        IBattleTeamRepresentation Team { get; }
        int Health { get; }
        int MaxHealth { get; }
        Vector2D Position { get; }
        float Radius { get; }
        Vector2D Speed { get; }

        StringBuilder GetDebugString();
    }

    public interface ITerrain
    {
        Vector2D Position { get; }
        float Radius { get; }
    }

    public interface IBattleProjectileRepresentation
    {
        Vector2D Position { get; }
        Vector2D Speed { get; }
    }

    public interface IBattleTeamRepresentation
    {
        int Id { get; }
        bool IsFriendly(IBattleTeamRepresentation team);
    }
}
