﻿using KM.Log;
using System;

namespace Z.KM.AIBattles.Common.Geometry
{
    public struct Vector2D
    {
        public float X;
        public float Y;

        public Vector2D(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static Vector2D Zero
        {
            get { return new Vector2D(0, 0); }
        }

        public static Vector2D AxisX
        {
            get
            {
                return new Vector2D(1, 0);
            }
        }

        public static Vector2D operator -(Vector2D value)
        {
            return new Vector2D(-value.X, -value.Y);
        }

        public static Vector2D operator +(Vector2D a, Vector2D b)
        {
            return new Vector2D(a.X + b.X, a.Y + b.Y);
        }

        public static Vector2D operator -(Vector2D a, Vector2D b)
        {
            return a + (-b);
        }

        public Vector2D LimitMagnitude(float maxMagnitude)
        {
            if (IsZero())
            {
                return this;
            }
            if (GetLengthSquare() <= maxMagnitude * maxMagnitude)
            {
                return this;
            }
            return Normalize(maxMagnitude);
        }

        public static Vector2D operator *(Vector2D a, float b)
        {
            return new Vector2D(a.X * b, a.Y * b);
        }

        public static Vector2D operator /(Vector2D a, float b)
        {
            return new Vector2D(a.X / b, a.Y / b);
        }

        public bool IsZero()
        {
            return X == 0 && Y == 0;
        }

        public float GetLength()
        {
            if (IsZero())
            {
                return 0;
            }
            return (float)Math.Sqrt(X * X + Y * Y);
        }

        public Vector2D Normalize()
        {
            if (IsZero())
            {
                return Zero;
            }
            var length = GetLength();
            return new Vector2D(X / length, Y / length);
        }

        public bool IsInRange(Vector2D target, float range)
        {
            return (this - target).GetLengthSquare() <= range * range;
        }

        public Vector2D Normalize(float length)
        {
            return Normalize() * length;
        }

        public float Dot(Vector2D other)
        {
            return X * other.X + Y * other.Y;
        }

        public float GetLengthSquare()
        {
            return X * X + Y * Y;
        }

        public override string ToString()
        {
            return string.Format("{0:F2}:{1:F2}", X, Y);
        }

        public bool Equals(Vector2D other)
        {
            return X == other.X && Y == other.Y;
        }

        static Vector2D()
        {
            Logger.TodoLowPriority("ToAnotherSystem и FromAnotherSystem сделано плохо (надо без тригонометрии и подумать куда базис должен указывать)");
        }

        /// <summary>
        /// повернуть в новую координатную систему.
        /// basis - это единичный вектор в старой системе координат,
        /// указывающий направление оси Y новой системы.
        /// x.ToAnotherSystem(new Vector2D(0,1)) == x;
        /// </summary>
        public Vector2D ToAnotherSystem(Vector2D basis)
        {
            var angle = -Math.Atan2(-basis.X, basis.Y); //повернули базис обратно, чтобы указывал на x
            var cos = (float)Math.Cos(angle);
            var sin = (float)Math.Sin(angle);
            var newX = X * cos - Y * sin;
            var newY = X * sin + Y * cos;
            return new Vector2D(newX, newY);
        }

        /// <summary>
        /// операция, обратная ToAnotherSystem, вращение в обратную сторону:
        /// x.FromAnotherSystem(y).ToAnotherSystem(y) == x;
        /// </summary>
        public Vector2D FromAnotherSystem(Vector2D basis)
        {
            var angle = Math.Atan2(-basis.X, basis.Y); //повернули базис обратно, чтобы указывал на x
            var cos = (float)Math.Cos(angle);
            var sin = (float)Math.Sin(angle);
            var newX = X * cos - Y * sin;
            var newY = X * sin + Y * cos;
            return new Vector2D(newX, newY);
        }
    }
}
