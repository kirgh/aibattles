using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Z.KM.AIBattles.Unity.Tourney;
using Z.KM.AIBattles.Unity.UI.Tourney;
using Z.KM.AIBattles.Unity.Utils;
using Logger = global::KM.Log.Logger;

namespace Z.KM.AIBattles.Unity
{
    public class Main : MonoBehaviour
    {

        [SerializeField]
        private Canvas _uiCanvas;
        [SerializeField]
        private Camera _camera;
        [SerializeField]
        private BattleView _battleViewPrefab;
        [SerializeField]
        private TourneyDialog _tourneyDialogPrefab;
        [SerializeField]
        private UnitSpritesCollection _unitSpritesCollection;

        private TourneyDialog _tourneyDialog;
        private BattleView _battleView;

        protected void Awake()
        {
            GUI.backgroundColor = Color.black;
            GUI.color = Color.white;
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;
            Logger.SetImplementation(new LoggerImplementation());
            _battleView = Instantiate(_battleViewPrefab, transform);
            _battleView.Init(_uiCanvas, _camera, OnBackFromBattle);
        }

        private void OnBackFromBattle()
        {
            _battleView.DisposeBattle();
            _tourneyDialog.Show();
        }

        protected void Start()
        {
            _tourneyDialog = Instantiate(_tourneyDialogPrefab, _uiCanvas.transform);
            Canvas.ForceUpdateCanvases();
            StartNewTourney();
        }

        private void StartNewTourney()
        {
            var teams = new List<Team>();
            teams.Add(Teams.Team1_Children);
            teams.Add(Teams.Team2_Doctors);
            teams.Add(Teams.Team3_Militia);
            teams.Add(Teams.Team4_Pioneer);
            teams.Add(Teams.Team5_Test);
            //for (var i = 0; i < 10; i++)
            //{
            //    teams.Add(new Team(i + 5, "test" + i, () => new FirstAiFactory()));
            //}
            //teams.Add(new Team(5, "test1", ()=> new FirstAiFactory()));
            //teams.Add(new Team(6, "test2", () => new FirstAiFactory()));

            TourneyMain.Settings tourneySettings;
            tourneySettings.IterationsBetweenTwoTeamsCount = 2;
            tourneySettings.DrawPoints = 1;
            tourneySettings.WinPoints = 3;
            tourneySettings.DrawWinPercentagesThreshold = 0.1f;
            tourneySettings.BattleSettingsFactory = BattleSettings.CreateFactory();
            TourneyDialog.ViewModel viewModel;
            teams.Shuffle(new System.Random());
            viewModel.Tourney = new TourneyMain(teams.AsReadOnly(), tourneySettings);
            viewModel.SpritesCollection = _unitSpritesCollection;
            viewModel.OnRestartClick = StartNewTourney;
            viewModel.OnPlayBattleClick = OnPlayBattleClick;
            viewModel.OnHustleClick = OnHustleClick;
            _tourneyDialog.ShowTourney(viewModel);
        }

        private void OnPlayBattleClick(ReadOnlyCollection<Team> teams, TourneyMain.Settings tourneySettings)
        {
            var settingsIndex = Random.Range(0, tourneySettings.BattleSettingsFactory.GetDifferentSettingsCount(teams.Count));
            var battleSettings = tourneySettings.BattleSettingsFactory.Create(teams, settingsIndex);
            var battle = Core.AIBattlesDefaultFactory.CreateBattle(battleSettings);
            _tourneyDialog.Hide();
            _battleView.ShowBattle(battle, teams);
        }

        private void OnHustleClick(ReadOnlyCollection<Team> teams, TourneyMain.Settings tourneySettings)
        {
            var settingsIndex = Random.Range(0, tourneySettings.BattleSettingsFactory.GetDifferentSettingsCount(teams.Count));
            var battleSettings = tourneySettings.BattleSettingsFactory.Create(teams, settingsIndex);
            var battle = Core.AIBattlesDefaultFactory.CreateBattle(battleSettings);
            _tourneyDialog.Hide();
            _battleView.ShowBattle(battle, teams);
        }

        protected void Update()
        {
            if (_battleView.IsFinished())
            {
                OnBackFromBattle();
            }
        }

        protected void OnGUI()
        {
            GUI.Box(new Rect(10, 10, 50, 25), new GUIContent((1 / Time.deltaTime).ToString("#.#")));
        }
    }
}