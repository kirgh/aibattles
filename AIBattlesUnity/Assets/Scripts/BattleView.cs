﻿using System;
using System.Collections.ObjectModel;
using UnityEngine;
using Z.KM.AIBattles.Common.Core;
using Z.KM.AIBattles.Unity.Utils;
using Z.KM.AIBattles.Unity.View;

namespace Z.KM.AIBattles.Unity
{
    public class BattleView : MonoBehaviour
    {


        [SerializeField]
        private VisibleUnit _unitPrefab;
        [SerializeField]
        private VisibleGameObject _projectilePrefab;
        [SerializeField]
        private VisibleObjectBottom _terrainPrefab;
        [SerializeField]
        private float _speed = 1f;
        [SerializeField]
        private float _cameraPadding = 5;
        [SerializeField]
        private float _minCameraSize = 20;
        [SerializeField]
        private TopDialog _topDialogPrefab;

        private const float PauseTime = 1;

        private Canvas _uiCanvas;
        private Camera _camera;
        private IBattle _battle;
        private Units _units;
        private VisibleObjectBottom _terrain;
        private Projectiles _projectiles;
        private CountDownTimer _pauseTimer;
        private CountDownTimer _battleUpdateTimer;
        private TopDialog _topDialog;

        public void ShowBattle(IBattle battle, ReadOnlyCollection<Team> teams)
        {
            _battleUpdateTimer.Set(BattleSettings.BattleTickInterval);
            _battle = battle;
            UpdateVisualization();
            _pauseTimer.Set(PauseTime);
            _topDialog.OnBattleStart(_battle.GetRepresentation(), teams);
            UpdateCamera(true);
        }

        internal void DisposeBattle()
        {
            _battle = null;
        }

        public bool IsFinished()
        {
            return _battle == null || (_battle.IsFinished() && _pauseTimer.IsFinished());
        }

        internal void Init(Canvas uiCanvas, Camera camera, Action onBack)
        {
            _camera = camera;
            _uiCanvas = uiCanvas;
            _topDialog = Instantiate(_topDialogPrefab, _uiCanvas.transform);
            _topDialog.Init(onBack);
            _terrain = Instantiate(_terrainPrefab);
            _units = new Units(_unitPrefab);
            _projectiles = new Projectiles(_projectilePrefab);
            _pauseTimer = new CountDownTimer();
            _battleUpdateTimer = new CountDownTimer();
            _camera.transform.position = Vector3.zero;
        }

        protected void Update()
        {
            var speed = Mathf.Clamp(_speed, 0, 10f);
            var updateTime = Time.deltaTime * speed;
            if (!_pauseTimer.IsFinished())
            {
                UpdateCamera(false);
                _pauseTimer.Update(Time.deltaTime); //тут специально, чтобы пауза от скорости не зависела
                return;
            }
            if (_battle == null)
            {
                return;
            }
            if (_battle.IsFinished())
            {
                _topDialog.OnBattleFinish(_battle.GetRepresentation());
                return;
            }
            _battleUpdateTimer.Update(updateTime);
            var startTime = Time.realtimeSinceStartup;
            const float maxTimeValue = 4f;//тормозим не более, чем во столько раз
            var maxTime = maxTimeValue * 1f / Application.targetFrameRate;
            while (!_battle.IsFinished() && _battleUpdateTimer.IsFinished() && Time.realtimeSinceStartup - startTime < maxTime)
            {
                _battle.NextStep();
                _battleUpdateTimer.Update(-BattleSettings.BattleTickInterval);
            }
            UpdateVisualization();
            if (_battle.IsFinished())
            {
                _pauseTimer.Set(PauseTime);
            }

            _topDialog.OnBattleUpdate(_battle.GetRepresentation());
        }


        private void UpdateVisualization()
        {
            var representation = _battle.GetRepresentation();
            _units.Update(representation.GetUnits());
            _projectiles.Update(representation.GetProjectiles());
            UpdateCamera(false);
            _terrain.Display(representation.GetTerrain().Radius, 1 - representation.SecondsFromStart / representation.MaxBattleTime);
            _terrain.transform.position = new Vector3(representation.GetTerrain().Position.X, representation.GetTerrain().Position.Y);
        }

        private void UpdateCamera(bool immediate)
        {
            var bounds = _units.GetBounds();
            if (bounds.Equals(Rect.zero))
            {
                return;
            }
            var desiredCameraPosition = new Vector3(bounds.center.x, bounds.center.y, -10);
            var minYSize = bounds.size.y + _cameraPadding;
            var minXSize = (bounds.size.x + _cameraPadding) / ((float)Screen.width / Screen.height);
            var desiredCameraSize = Mathf.Max(minXSize, minYSize, _minCameraSize) / 2;
            var lerpValue = immediate ? 1 : 0.1f;
            _camera.transform.position = Vector3.Lerp(_camera.transform.position, desiredCameraPosition, lerpValue);
            _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, desiredCameraSize, lerpValue);
        }
    }
}
