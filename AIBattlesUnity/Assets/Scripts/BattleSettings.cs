﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using Z.KM.AIBattles.Common.Core;
using Z.KM.AIBattles.Common.Geometry;
using Z.KM.AIBattles.Unity.Utils;

namespace Z.KM.AIBattles.Unity
{
    public class BattleSettings
    {

        public const float BattleTickInterval = 1f / 60;

        public static IBattleSettingsFactory CreateFactory()
        {
            return new Factory();
        }

        private class Factory : IBattleSettingsFactory
        {
            private const int maxHealth = 16; //HEALTH!!!

            private readonly List<float> XMultipliers = new List<float>
            {
                0.3f,
                0.6f
            };

            public CreateBattleSettings Create(ReadOnlyCollection<Team> teams, int zeroBasedSettingsIndex)
            {
                if (zeroBasedSettingsIndex >= GetDifferentSettingsCount(teams.Count) || zeroBasedSettingsIndex < 0)
                {
                    global::KM.Log.Logger.Error("BattleSettings.Factory wrong settings index");
                    return Create(teams, 0);
                }

                CreateBattleSettings settings;
                settings.ProjectileDamage = 4; //DAMAGE!!!
                settings.MaxBattleTime = 15; //BATTLE_TIME!!!
                settings.StartShrinkTerrainTime = settings.MaxBattleTime * 0.75f;
                settings.TickInterval = BattleTickInterval;
                settings.UnitRadius = 0.6f;
                settings.UnitsMaxSpeed = 6;
                settings.Energy.MaxEnergy = 6;
                settings.Energy.IncreaseSpeed = 6;
                settings.Energy.MoveDecreaseSpeed = 1;
                settings.Energy.AttackCost = 5;
                settings.ProjectileSpeed = 40;
                settings.ProjectileLifeTime = 2;
                settings.UnitDeceleration = 40;
                settings.UnitAcceleration = 40;
                settings.ProjectilePushTargetCoefficient = 0.125f;
                settings.TerrainRadius = 15;
                settings.TerrainPosition = new Vector2D(100, 100);

                List<CreateBattleSettings.Team> settingTeams;
                if (teams.Count != 2)
                {
                    var shuffledTeams = teams.ToList();
                    var random = new System.Random();
                    shuffledTeams.Shuffle(random);
                    settingTeams = new List<CreateBattleSettings.Team>();
                    for (var teamIndex = 0; teamIndex < teams.Count; teamIndex++)
                    {
                        const int teamSize = 5;
                        settingTeams.Add(CreateTeamRandom(
                            teams[teamIndex],
                            teamSize,
                            settings.TerrainPosition,
                            settings.TerrainRadius,
                            maxHealth,
                            random)
                        );
                    }

                }
                else
                {
                    var teamA = zeroBasedSettingsIndex % 2 == 0 ? teams[0] : teams[1];
                    var teamB = zeroBasedSettingsIndex % 2 == 0 ? teams[1] : teams[0];


                    settingTeams = new List<CreateBattleSettings.Team>();
                    var multiplierIndex = (zeroBasedSettingsIndex / 2) % XMultipliers.Count;


                    if (zeroBasedSettingsIndex >= teams.Count * XMultipliers.Count * 2)
                    {
                        const int teamSize = 5;
                        var random = new System.Random(0);
                        settingTeams.Add(CreateTeamRandom(
                            teamA,
                            teamSize,
                            settings.TerrainPosition,
                            settings.TerrainRadius,
                            maxHealth,
                            random)
                        );
                        settingTeams.Add(CreateTeamRandom(
                            teamB,
                            teamSize,
                            settings.TerrainPosition,
                            settings.TerrainRadius,
                            maxHealth,
                            random)
                        );
                    }
                    else if (zeroBasedSettingsIndex >= teams.Count * XMultipliers.Count)
                    {
                        const int teamSize = 5;
                        settingTeams.Add(CreateTeam1(teamA, teamSize, settings.TerrainPosition, settings.TerrainRadius, false, maxHealth, XMultipliers[multiplierIndex]));
                        settingTeams.Add(CreateTeam1(teamB, teamSize, settings.TerrainPosition, settings.TerrainRadius, true, maxHealth, XMultipliers[multiplierIndex]));
                    }
                    else
                    {
                        const int teamSize = 9;
                        settingTeams.Add(CreateTeam2(teamA, teamSize, settings.TerrainPosition, settings.TerrainRadius, false, maxHealth, XMultipliers[multiplierIndex]));
                        settingTeams.Add(CreateTeam2(teamB, teamSize, settings.TerrainPosition, settings.TerrainRadius, true, maxHealth, XMultipliers[multiplierIndex]));
                    }
                }

                settings.Teams = settingTeams.AsReadOnly();
                settings.RandomSeed = 0;
                return settings;
            }

            public int GetDifferentSettingsCount(int teamsCount)
            {
                return GetReplacementsCount(teamsCount) * GetSettingsCount(teamsCount);
            }

            private static int GetReplacementsCount(int teamsCount)
            {
                int result = 1;
                for (var i = teamsCount; i > 1; i--)
                {
                    result *= i;
                }
                return result;
            }

            private int GetSettingsCount(int teamsCount)
            {
                if (teamsCount == 2)
                {
                    return XMultipliers.Count * 2 + 1;
                }
                return 1;
            }

            private static CreateBattleSettings.Team CreateTeam1(Team team, int teamSize, Vector2D terrainPosition, float terrainRadius, bool right, int maxHealth, float xMultiplier)
            {
                CreateBattleSettings.Team result;
                result.Id = team.Id;
                result.AI = team.CreateAiFactory();

                var units = new List<CreateBattleSettings.Unit>();
                for (var i = 0; i < teamSize; i++)
                {
                    CreateBattleSettings.Unit unit;
                    unit.MaxHealth = maxHealth;
                    unit.Position = terrainPosition + new Vector2D(
                        terrainRadius * xMultiplier * (right ? 1 : -1),
                        ((float)i / teamSize - 0.5f) * terrainRadius * 0.71f
                    );
                    units.Add(unit);
                }
                result.Units = units.AsReadOnly();
                return result;
            }

            private static CreateBattleSettings.Team CreateTeam2(Team team, int teamSize, Vector2D terrainPosition, float terrainRadius, bool right, int maxHealth, float xMultiplier)
            {
                CreateBattleSettings.Team result;
                result.Id = team.Id;
                result.AI = team.CreateAiFactory();

                var units = new List<CreateBattleSettings.Unit>();
                for (var i = 0; i < teamSize; i++)
                {
                    CreateBattleSettings.Unit unit;
                    unit.MaxHealth = maxHealth;
                    unit.Position = terrainPosition + new Vector2D(
                        terrainRadius * xMultiplier * (right ? 1 : -1),
                        0
                    );

                    var squadSize = Mathf.CeilToInt(Mathf.Sqrt(teamSize));
                    var column = i / squadSize;
                    var row = i % squadSize;
                    var cellSize = terrainRadius * 0.4f / squadSize;
                    unit.Position += new Vector2D(column * cellSize - squadSize * 0.5f, row * cellSize - squadSize * 0.5f);
                    units.Add(unit);
                }
                result.Units = units.AsReadOnly();
                return result;
            }

            private static CreateBattleSettings.Team CreateTeamRandom(Team team, int teamSize, Vector2D terrainPosition, float terrainRadius, int maxHealth, System.Random random)
            {
                CreateBattleSettings.Team result;
                result.AI = team.CreateAiFactory();
                result.Id = team.Id;
                var units = new List<CreateBattleSettings.Unit>();

                for (var unitIndex = 0; unitIndex < teamSize; unitIndex++)
                {
                    CreateBattleSettings.Unit unit;
                    unit.MaxHealth = maxHealth;
                    var angle = random.NextDouble() * Math.PI * 2;
                    var radius = terrainRadius * 0.9f * (float)Math.Sqrt(random.NextDouble());
                    unit.Position = terrainPosition + new Vector2D(
                        radius * (float)Math.Cos(angle),
                        radius * (float)Math.Sin(angle)
                    );
                    units.Add(unit);
                }
                result.Units = units.AsReadOnly();
                return result;
            }

            //private static void SmthToDelete()
            //{
            //    Logger.TodoLowPriority("Start positions and terrain is fake!");
            //    var random = new System.Random();
            //    var useNewOrientation = random.NextDouble() > 0.5;
            //    for (var teamIndex = 0; teamIndex < teams.Count; teamIndex++)
            //    {
            //        var team = teams[teamIndex];
            //        for (var teamUnitIndex = 0; teamUnitIndex < settings.TeamSize; teamUnitIndex++)
            //        {
            //            var gameObject = new GameObject();

            //            double angle;
            //            double range;
            //            if (!useNewOrientation)
            //            {
            //                angle = random.NextDouble() * Math.PI * 2;
            //                range = random.NextDouble() * terrainComponent.Radius * 0.9f;
            //            }
            //            else
            //            {
            //                angle = (teamIndex * settings.TeamSize + teamUnitIndex) * Math.PI * 2 / (settings.TeamSize * teams.Count);
            //                range = terrainComponent.Radius * 0.75f;
            //            }
            //            var position = terrainComponent.Position + new Vector2D((float)(range * Math.Cos(angle)), (float)(range * Math.Sin(angle)));
            //            gameObject.AddComponent(new PositionComponent(position));
            //            gameObject.AddComponent(new MovableComponent(Vector2D.Zero));
            //            gameObject.AddComponent(new HealthComponent(settings.StartUnitsHealth));
            //            gameObject.AddComponent(new BodyComponent(settings.UnitRadius));
            //            gameObject.AddComponent(new CasterComponent(settings.MeleeDamage, settings.MeleeRange, settings.ProjectileSpeed, settings.Energy.AttackCost, settings.ProjectileLifeTime, settings.ProjectilePushTargetCoefficient));
            //            PerceptionSystem.InitUnitComponents(gameObject);
            //            CreateUnitAiData createUnitData;
            //            createUnitData.UniqueIdInTeam = teamUnitIndex;
            //            gameObject.AddComponent(new AIComponent(team.AI.CreateUnitAi(createUnitData)));
            //            gameObject.AddComponent(new BattleUnitComponent(team));
            //            gameObject.AddComponent(new EnergyComponent(settings.Energy));
            //            gameObject.AddComponent(new SelfMovableComponent(settings.UnitDeceleration, settings.UnitAcceleration, settings.UnitsMaxSpeed));
            //            AddObject(gameObject);
            //        }
            //    }
            //}
        }
    }
}
