﻿using Z.KM.AIBattles.TestAi;
using Z.KM.AIBattles.Unity.AI;

namespace Z.KM.AIBattles.Unity
{
    public static class Teams
    {
        public static Team Team1_Children = new Team(0, "Дети", () => new FirstAiFactory());
        public static Team Team2_Doctors = new Team(1, "Санитары", () => new SecondAiFactory());
        public static Team Team3_Militia = new Team(2, "Милиционеры", () => new ThirdAiFactory(0.9f));
        public static Team Team4_Pioneer = new Team(3, "Пионеры", () => new FourthAiFactory());
        public static Team Team5_Test = new Team(4, "Тестовая", () => new TestAiFactory());
    }
}
