﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using Z.KM.AIBattles.Common.Core;
using Z.KM.AIBattles.Unity.Utils;

namespace Z.KM.AIBattles.Unity.Tourney
{
    /// <summary>
    /// основной класс логики для проведения турнира между командами
    /// </summary>
    public class TourneyMain
    {

        public struct Settings
        {
            /// <summary>
            /// сколько итераций боёв будет проведено каждой командой с каждой другой.
            /// одна итерация предполагает несколько боёв с разными стартовыми настройками, в тч перемена мест.
            /// </summary>
            public int IterationsBetweenTwoTeamsCount;

            /// <summary>
            /// сколько очков получает команда-победитель
            /// </summary>
            public int WinPoints;

            /// <summary>
            /// сколько очков получают обе команды в случае ничьи
            /// </summary>
            public int DrawPoints;

            /// <summary>
            /// сколько процентов разницы считаются ничьёй. например, A и B сыграли так:
            /// A выиграла в 30%, B выиграла в 20%, в остальных случаях - ничья.
            /// teamAWins = teamAWinPercentage > teamBWinPercentage && teamAWinPercentage - teamBWinPercentage > DrawWinPercentagesThreshold;
            /// если DrawWinPercentagesThreshold >= 10% то в результате засчитают ничью.
            /// </summary>
            public float DrawWinPercentagesThreshold;

            /// <summary>
            /// создание настроек боя
            /// </summary>
            public IBattleSettingsFactory BattleSettingsFactory;

            public int GetTotalBattlesCountBetweenTwoTeams()
            {
                return IterationsBetweenTwoTeamsCount * BattleSettingsFactory.GetDifferentSettingsCount(2);
            }
        }

        private struct CurrentBattle
        {
            public Team TeamA;
            public Team TeamB;
            public IBattle Battle;
            public int CurrentCompletedBattlesCount;
            public int TeamAWinsCount;
            public int TeamBWinsCount;

            public void Reset()
            {
                TeamA = null;
                TeamB = null;
                Battle = null;
                CurrentCompletedBattlesCount = 0;
                TeamAWinsCount = 0;
                TeamBWinsCount = 0;
            }

            internal bool IsBattling(Team team, Team otherTeam)
            {
                return (TeamA == team && TeamB == otherTeam) || (TeamB == team && TeamA == otherTeam);
            }
        }

        private readonly ReadOnlyCollection<Team> _teams;
        private readonly Dictionary<Team, TeamHistory> _teamToTeamHistory;
        private readonly Settings _settings;
        private CurrentBattle _currentBattle;

        public TourneyMain(ReadOnlyCollection<Team> teams, Settings settings)
        {
            _settings = settings;
            _teams = teams;
            _teamToTeamHistory = new Dictionary<Team, TeamHistory>();
            foreach (var team in _teams)
            {
                _teamToTeamHistory[team] = new TeamHistory();
            }
        }

        public bool IsFinished()
        {
            return _teamToTeamHistory.All(teamPlayRecords => teamPlayRecords.Value.BattlesWithOtherTeams.Count == _teams.Count - 1);
        }

        internal ReadOnlyCollection<Team> GetTeams()
        {
            return _teams;
        }

        public void Update()
        {
            if (IsFinished())
            {
                return;
            }

            if (_currentBattle.TeamA == null)
            {
                //вообще не выбраны команды, которые сейчас сражаются  (не стартовал, или предыдущая пара отыграла)
                _currentBattle.TeamA = GetNextTeamWithMinimumPlayCount();
                _currentBattle.TeamB = GetOpponent(_currentBattle.TeamA);
                _currentBattle.Battle = null;
            }

            var differentSettingsCount = _settings.BattleSettingsFactory.GetDifferentSettingsCount(2);
            if (_currentBattle.Battle == null)
            {
                //команды выбраны, но боя нет (не стартовал, или уже закончился)
                var teams = new List<Team>();
                teams.Add(_currentBattle.TeamA);
                teams.Add(_currentBattle.TeamB);
                var settingsIndex = _currentBattle.CurrentCompletedBattlesCount % differentSettingsCount;
                var settings = _settings.BattleSettingsFactory.Create(teams.AsReadOnly(), settingsIndex);
                _currentBattle.Battle = Core.AIBattlesDefaultFactory.CreateBattle(settings);
            }

            var currentTime = Time.realtimeSinceStartup;
            var maxTime = 2f / Application.targetFrameRate;
            while (!_currentBattle.Battle.IsFinished() && Time.realtimeSinceStartup - currentTime < maxTime)
            {
                _currentBattle.Battle.NextStep();
            }

            if (!_currentBattle.Battle.IsFinished())
            {
                //бой остался не завершённый - выходим до следующего Update
                return;
            }

            //бой завершён - записываем в _currentBattle
            var representation = _currentBattle.Battle.GetRepresentation();
            var hasTeamAUnits = representation.GetUnits().Any(unit => unit.Health > 0 && unit.Team.Id == _currentBattle.TeamA.Id);
            var hasTeamBUnits = representation.GetUnits().Any(unit => unit.Health > 0 && unit.Team.Id == _currentBattle.TeamB.Id);
            _currentBattle.CurrentCompletedBattlesCount++;
            if (hasTeamAUnits && !hasTeamBUnits)
            {
                _currentBattle.TeamAWinsCount++;
            }
            else if (hasTeamBUnits && !hasTeamAUnits)
            {
                _currentBattle.TeamBWinsCount++;
            }

            _currentBattle.Battle = null;
            var totalBattlesCount = _settings.IterationsBetweenTwoTeamsCount * differentSettingsCount;
            if (_currentBattle.CurrentCompletedBattlesCount >= totalBattlesCount)
            {
                //все бои рассчитали, сохраняем в историю и сбрасываем полностью
                var drawCount = totalBattlesCount - _currentBattle.TeamAWinsCount - _currentBattle.TeamBWinsCount;

                PlayRecord teamARecord;
                teamARecord.WinCount = _currentBattle.TeamAWinsCount;
                teamARecord.DrawCount = drawCount;
                PlayRecord teamBRecord;
                teamBRecord.WinCount = _currentBattle.TeamBWinsCount;
                teamBRecord.DrawCount = drawCount;

                _teamToTeamHistory[_currentBattle.TeamA].BattlesWithOtherTeams[_currentBattle.TeamB] = teamARecord;
                _teamToTeamHistory[_currentBattle.TeamB].BattlesWithOtherTeams[_currentBattle.TeamA] = teamBRecord;
                _currentBattle.Reset();
            }
        }

        internal Settings GetSettings()
        {
            return _settings;
        }

        internal Dictionary<Team, TeamHistory> GetResult()
        {
            return _teamToTeamHistory;
        }

        private Team GetNextTeamWithMinimumPlayCount()
        {
            return _teamToTeamHistory
                .ToList()
                .OrderBy(pair => pair.Value.BattlesWithOtherTeams.Count)
                .FirstOrDefault().Key;
        }

        private Team GetOpponent(Team otherTeam)
        {
            return _teamToTeamHistory
                .ToList()
                .Where(pair => pair.Key != otherTeam)
                .Where(pair => !pair.Value.HasPlayedWith(otherTeam))
                .FirstOrDefault().Key;
        }

        public class TeamHistory
        {
            public readonly Dictionary<Team, PlayRecord> BattlesWithOtherTeams = new Dictionary<Team, PlayRecord>();

            internal bool HasPlayedWith(Team otherTeam)
            {
                return BattlesWithOtherTeams.ContainsKey(otherTeam);
            }
        }

        public struct PlayRecord
        {
            public int WinCount;
            public int DrawCount;
        }

        public float GetFinishPercents()
        {
            var completedBattles = _teamToTeamHistory.Sum(teamPlayRecords => teamPlayRecords.Value.BattlesWithOtherTeams.Count);
            var totalBattles = _teams.Count * (_teams.Count - 1);
            var result = (float)completedBattles / totalBattles;
            if (_currentBattle.TeamA != null)
            {
                result += (float)_currentBattle.CurrentCompletedBattlesCount / (_settings.GetTotalBattlesCountBetweenTwoTeams() * totalBattles);
            }

            return result;
        }

        internal bool IsBattleInProgress(Team team, Team otherTeam)
        {
            return _currentBattle.IsBattling(team, otherTeam);
        }
    }
}
