﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Z.KM.AIBattles.Unity.Tourney
{
    public class TourneyMainRepresentation
    {

        public struct TeamWithScore
        {
            public Team Team;
            public int GainedScorePerTeam;
            public int GainedScorePerBattles;
            public int ExpectedScorePerTeam;
        }

        /// <summary>
        /// это на самом деле не 1 Battle, а весь набор боёв на всех стартовых настройках
        /// </summary>
        public struct BattleWithAnotherTeam
        {
            public enum StateType
            {
                SameTeam,
                NotStarted,
                InProgressNow,
                Completed
            }

            public enum ResultType
            {
                Win,
                Draw,
                Lose
            }

            public StateType State;
            public int GainedScorePerTeam;
            public int GainedScorePerBattles;
            public float WinPercentage;
            public int WinCount;
            public int DrawCount;
            public ResultType Result;

            internal static BattleWithAnotherTeam CreateSameTeam()
            {
                BattleWithAnotherTeam result;
                result.State = StateType.SameTeam;
                result.GainedScorePerTeam = 0;
                result.GainedScorePerBattles = 0;
                result.WinPercentage = 0;
                result.WinCount = 0;
                result.DrawCount = 0;
                result.Result = ResultType.Draw;
                return result;
            }

            internal static BattleWithAnotherTeam CreateNotStartedOrInProgress(bool inProgressNow)
            {
                BattleWithAnotherTeam result;
                result.State = inProgressNow ? StateType.InProgressNow : StateType.NotStarted;
                result.GainedScorePerTeam = 0;
                result.GainedScorePerBattles = 0;
                result.WinPercentage = 0;
                result.WinCount = 0;
                result.DrawCount = 0;
                result.Result = ResultType.Draw;
                return result;
            }

            internal static BattleWithAnotherTeam CreateCompleted(ResultType resultType, int scorePerTeam, int scorePerBattles, float winPercentage, int winCount, int drawCount)
            {
                BattleWithAnotherTeam result;
                result.State = StateType.Completed;
                result.GainedScorePerTeam = scorePerTeam;
                result.GainedScorePerBattles = scorePerBattles;
                result.WinPercentage = winPercentage;
                result.WinCount = winCount;
                result.DrawCount = drawCount;
                result.Result = resultType;
                return result;
            }
        }

        private readonly TourneyMain _tourney;

        public int IterationsBetweenTwoTeamsCount { get { return _tourney.GetSettings().IterationsBetweenTwoTeamsCount; } }

        public TourneyMainRepresentation(TourneyMain tourney)
        {
            _tourney = tourney;
        }

        public BattleWithAnotherTeam GetBattleBetweenTeams(Team team, Team otherTeam)
        {

            if (team == otherTeam)
            {
                return BattleWithAnotherTeam.CreateSameTeam();
            }

            var teamHistory = _tourney.GetResult()[team];

            TourneyMain.PlayRecord result;
            if (!teamHistory.BattlesWithOtherTeams.TryGetValue(otherTeam, out result))
            {
                return BattleWithAnotherTeam.CreateNotStartedOrInProgress(_tourney.IsBattleInProgress(team, otherTeam));
            }

            var settings = _tourney.GetSettings();
            var winPercentage = (float)result.WinCount / settings.GetTotalBattlesCountBetweenTwoTeams();
            var anotherTeamPercentage = (float)(settings.GetTotalBattlesCountBetweenTwoTeams() - result.WinCount - result.DrawCount) / settings.GetTotalBattlesCountBetweenTwoTeams();
            var allBattlesScore = result.WinCount * settings.WinPoints + result.DrawCount * settings.DrawPoints;
            int averageBattlesScore;
            BattleWithAnotherTeam.ResultType resultType;
            if (winPercentage > anotherTeamPercentage + settings.DrawWinPercentagesThreshold)
            {
                averageBattlesScore = settings.WinPoints;
                resultType = BattleWithAnotherTeam.ResultType.Win;
            }
            else if (anotherTeamPercentage > winPercentage + settings.DrawWinPercentagesThreshold)
            {
                averageBattlesScore = 0;
                resultType = BattleWithAnotherTeam.ResultType.Lose;
            }
            else
            {
                averageBattlesScore = settings.DrawPoints;
                resultType = BattleWithAnotherTeam.ResultType.Draw;
            }

            return BattleWithAnotherTeam.CreateCompleted(resultType, averageBattlesScore, allBattlesScore, winPercentage, result.WinCount, result.DrawCount);
        }

        internal float GetFinishPercents()
        {
            return _tourney.GetFinishPercents();
        }

        internal int GetSettingsPerTwoTeamCount()
        {
            return _tourney.GetSettings().BattleSettingsFactory.GetDifferentSettingsCount(2);
        }

        internal bool IsFinished()
        {
            return _tourney.IsFinished();
        }

        public ReadOnlyCollection<TeamWithScore> CreateTeams(bool sorted)
        {
            var resultList = new List<TeamWithScore>();
            var teams = _tourney.GetTeams();
            var teamsCount = teams.Count;
            var settings = _tourney.GetSettings();
            for (var i = 0; i < teamsCount; i++)
            {
                var team = teams[i];
                int gainedScorePerTeam = 0;
                int gainedScorePerBattles = 0;
                for (var j = 0; j < teamsCount; j++)
                {
                    var otherTeam = teams[j];
                    var battleResult = GetBattleBetweenTeams(team, otherTeam);
                    gainedScorePerTeam += battleResult.GainedScorePerTeam;
                    gainedScorePerBattles += battleResult.GainedScorePerBattles;
                }
                TeamWithScore teamWithScore;
                teamWithScore.Team = team;
                teamWithScore.GainedScorePerTeam = gainedScorePerTeam;
                teamWithScore.GainedScorePerBattles = gainedScorePerBattles;
                var teamHistory = _tourney.GetResult()[team];
                var leftBattles = settings.GetTotalBattlesCountBetweenTwoTeams() - teamHistory.BattlesWithOtherTeams.Count;
                teamWithScore.ExpectedScorePerTeam = gainedScorePerTeam + settings.WinPoints * leftBattles / 2;
                resultList.Add(teamWithScore);
            }
            if (sorted)
            {
                resultList.Sort(SortTeams);
            }
            return resultList.AsReadOnly();
        }

        private static int SortTeams(TeamWithScore a, TeamWithScore b)
        {
            if (a.ExpectedScorePerTeam != b.ExpectedScorePerTeam)
            {
                return a.ExpectedScorePerTeam > b.ExpectedScorePerTeam ? -1 : 1;
            }
            if (a.GainedScorePerTeam != b.GainedScorePerTeam)
            {
                return a.GainedScorePerTeam > b.GainedScorePerTeam ? -1 : 1;
            }
            if (a.GainedScorePerBattles != b.GainedScorePerBattles)
            {
                return a.GainedScorePerBattles > b.GainedScorePerBattles ? -1 : 1;
            }
            return 0;
        }

    }
}
