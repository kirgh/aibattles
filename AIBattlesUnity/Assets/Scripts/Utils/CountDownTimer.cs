﻿namespace Z.KM.AIBattles.Unity.Utils
{
    /// <summary>
    /// класс, чтобы можно было легко отсчитывать какой-то интервал времени:
    /// задаёшь время (Set) и апдейтишь (Update), пока оно не закончится (IsFinished)
    /// </summary>

    public class CountDownTimer
    {
        private float _leftSeconds;

        public void Reset()
        {
            _leftSeconds = 0;
        }

        public void Set(float seconds)
        {
            _leftSeconds = seconds;
        }

        public bool IsFinished()
        {
            return _leftSeconds <= 0;
        }

        public void Update(float delta)
        {
            _leftSeconds -= delta;
        }
    }
}