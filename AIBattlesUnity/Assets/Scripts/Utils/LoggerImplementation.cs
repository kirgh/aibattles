﻿using KM.Log;
using UnityEngine;

namespace Z.KM.AIBattles.Unity.Utils
{
    internal class LoggerImplementation : ILoggerImplementation
    {
        public void Error(string s)
        {
            Debug.LogError(s);
        }

        public void Log(string s)
        {
            Debug.Log(s);
        }

        public void LogFormat(string format, params object[] args)
        {
            Debug.LogFormat(format, args);
        }

        public void Warning(string s)
        {
            Debug.LogWarning(s);
        }
    }
}