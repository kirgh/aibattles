﻿namespace Z.KM.AIBattles.Unity.Utils
{
    /// <summary>
    /// повторяющийся таймер.
    /// создаёшь с интервалом, делаешь Update() на прошедшее время, проверяешь готовность (IsReady),
    /// и если да - делаешь свою логику и вызываешь Tick()
    /// </summary>
    class RepeatedTimer
    {
        private readonly float _interval;
        private float _time;

        public RepeatedTimer(float tickInterval, bool tickImmediately)
        {
            _interval = tickInterval;
            _time = tickImmediately ? tickInterval : 0;
        }

        public bool IsReady()
        {
            return _time >= _interval;
        }

        public void Tick()
        {
            _time -= _interval;
        }

        public void Update(float deltaTime)
        {
            _time += deltaTime;
        }
    }
}