﻿using System;
using Z.KM.AIBattles.Common.AI;

namespace Z.KM.AIBattles.Unity
{
    public class Team
    {
        public readonly int Id;
        public readonly string Name;

        private readonly Func<IAiFactory> _factoryConstructor;

        public Team(int id, string name, Func<IAiFactory> factoryConstructor)
        {
            Id = id;
            Name = name;
            _factoryConstructor = factoryConstructor;
        }

        public int UnitSpriteIndex { get { return Id; } }

        public IAiFactory CreateAiFactory()
        {
            return _factoryConstructor();
        }

        internal Team CloneForSelfAttack()
        {
            return new Team(Id == 0 ? 1 : Id - 1, Name + " (copy)", _factoryConstructor);
        }
    }
}
