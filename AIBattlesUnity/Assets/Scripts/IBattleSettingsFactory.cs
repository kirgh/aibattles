﻿using System.Collections.ObjectModel;
using Z.KM.AIBattles.Common.Core;

namespace Z.KM.AIBattles.Unity
{
    /// <summary>
    /// создаёт настройки для команд со всеми перестановками и со всеми вариантами стартовых настроек.
    /// то есть не надо сюда отдавать teams в разном порядке, оно само разберётся внутри.
    /// </summary>
    public interface IBattleSettingsFactory
    {
        CreateBattleSettings Create(ReadOnlyCollection<Team> teams, int zeroBasedSettingsIndex);
        int GetDifferentSettingsCount(int teamsCount);
    }
}
