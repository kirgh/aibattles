﻿using UnityEngine;

namespace Z.KM.AIBattles.Unity
{
    public class UnitSpritesCollection : MonoBehaviour
    {
        [SerializeField]
        private Sprite[] _sprites;

        public Sprite GetSpriteForTeamId(int teamId)
        {
            if(teamId < 0 || teamId >= _sprites.Length)
            {
                global::KM.Log.Logger.Error("no sprite for teamId " + teamId);
                return null;
            }
            return _sprites[teamId];
        }
    }
}
