﻿using System;
using System.Linq;
using Z.KM.AIBattles.Common.AI;

namespace Z.KM.AIBattles.Unity.AI
{
    public class FirstAiFactory : IAiFactory
    {
        public IBattleUnitAi CreateUnitAi(CreateUnitAiData data)
        {
            return new FirstAiFactoryUnitAi();
        }

        public void Init(InitAiFactoryData data)
        {
            //nothing
        }

        private class FirstAiFactoryUnitAi : IBattleUnitAi
        {
            public bool VisbileUnit { get; private set; }

            public UpdateUnitResult Update(UpdateUnitAiData data)
            {
                UpdateUnitResult result;

                var firstEnemy = data.Perception.VisibleUnits
                    .Where(unit => !unit.Friendly)
                    .FirstOrDefault();

                if (firstEnemy != null)
                {
                    if (data.Perception.Energy < data.Perception.ThrowEnergyCost)
                    {
                        result.Attack = default(UpdateUnitResult.AttackDesire);
                    }
                    else
                    {
                        UpdateUnitResult.AttackDesire attack;
                        attack.Enabled = true;
                        attack.Direction = firstEnemy.Direction;
                        result.Attack = attack;
                    }
                    UpdateUnitResult.MoveDesire move;
                    move.Enabled = true;
                    move.Direction = firstEnemy.Direction.Normalize();
                    result.Move = move;
                }
                else
                {
                    result.Attack = default(UpdateUnitResult.AttackDesire);
                    result.Move = default(UpdateUnitResult.MoveDesire);
                }

                if (data.Perception.VectorToNearestTerrainEdge.GetLengthSquare() < 5)
                {
                    result.Move.Enabled = true;
                    result.Move.Direction += -data.Perception.VectorToNearestTerrainEdge.Normalize(5 - data.Perception.VectorToNearestTerrainEdge.GetLengthSquare());
                }

                return result;
            }
        }
    }
}