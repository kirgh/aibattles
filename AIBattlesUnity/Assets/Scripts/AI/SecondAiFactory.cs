﻿using System.Linq;
using Z.KM.AIBattles.Common.AI;

namespace Z.KM.AIBattles.Unity.AI
{
    public class SecondAiFactory : IAiFactory
    {
        public IBattleUnitAi CreateUnitAi(CreateUnitAiData data)
        {
            return new SecondAiFactoryUnitAi();
        }

        public void Init(InitAiFactoryData data)
        {
            //nothing
        }

        private class SecondAiFactoryUnitAi : IBattleUnitAi
        {
            public UpdateUnitResult Update(UpdateUnitAiData data)
            {
                UpdateUnitResult result;
                var nearest = data.Perception.VisibleUnits
                    .Where(unit => !unit.Friendly)
                    .OrderBy(unit => unit.Direction.GetLengthSquare())
                    .FirstOrDefault();
                if (nearest == null)
                {
                    result.Attack = default(UpdateUnitResult.AttackDesire);
                    result.Move = default(UpdateUnitResult.MoveDesire);
                    return result;
                }
                if (data.Perception.Energy < data.Perception.ThrowEnergyCost)
                {
                    result.Attack = default(UpdateUnitResult.AttackDesire);
                }
                else
                {
                    result.Attack.Enabled = true;
                    result.Attack.Direction = nearest.Direction;
                }
                result.Move.Enabled = true;
                result.Move.Direction = nearest.Direction;
                return result;
            }
        }
    }
}