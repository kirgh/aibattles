﻿using System.Linq;
using Z.KM.AIBattles.Common.AI;
using Z.KM.AIBattles.Common.Geometry;

namespace Z.KM.AIBattles.Unity.AI
{
    public class FourthAiFactory : IAiFactory
    {
        public IBattleUnitAi CreateUnitAi(CreateUnitAiData data)
        {
            return new FourthUnitAi();
        }

        public void Init(InitAiFactoryData data)
        {
            //nothing
        }

        private class FourthUnitAi : IBattleUnitAi
        {

            public UpdateUnitResult Update(UpdateUnitAiData data)
            {
                UpdateUnitResult result;

                if (data.Perception.IsTerrainShrinking)
                {
                    result.Move.Enabled = true;
                    result.Move.Direction = data.Perception.VectorToNearestTerrainEdge.Normalize() * -1;
                }
                else
                {
                    result.Move.Enabled = true;
                    if (((int)(data.Perception.TimeFromStart * 5)) % 5 > 0)
                    {
                        result.Move.Direction = data.Perception.CurrentSpeed.Equals(Vector2D.Zero)
                            ? Vector2D.AxisX
                            : data.Perception.CurrentSpeed.ToAnotherSystem(new Vector2D(0.71f, 0.71f)).Normalize(1);
                    }
                    else
                    {
                        result.Move.Direction = -data.Perception.CurrentSpeed;
                    }
                }

                if (data.Perception.Energy < data.Perception.ThrowEnergyCost)
                {
                    result.Attack = default(UpdateUnitResult.AttackDesire);
                }
                else
                {
                    var nearestEnemy = data.Perception.VisibleUnits
                        .Where(unit => !unit.Friendly)
                        .OrderBy(unit => unit.Direction.GetLengthSquare())
                        .FirstOrDefault();
                    if (nearestEnemy != null)
                    {
                        result.Attack.Enabled = true;
                        result.Attack.Direction = nearestEnemy.Direction;
                    }
                    else
                    {
                        result.Attack = default(UpdateUnitResult.AttackDesire);
                    }
                }

                if (data.Perception.VectorToNearestTerrainEdge.GetLengthSquare() < 5)
                {
                    result.Move.Enabled = true;
                    result.Move.Direction += -data.Perception.VectorToNearestTerrainEdge.Normalize(5 - data.Perception.VectorToNearestTerrainEdge.GetLengthSquare());
                }

                return result;
            }
        }
    }
}
