﻿using System.Linq;
using Z.KM.AIBattles.Common.AI;

namespace Z.KM.AIBattles.Unity.AI
{
    public class ThirdAiFactory : IAiFactory
    {
        private readonly float _evasion;

        public ThirdAiFactory(float evasion)
        {
            _evasion = evasion;
        }

        public IBattleUnitAi CreateUnitAi(CreateUnitAiData data)
        {
            return new ThirdAiFactoryUnitAi(_evasion);
        }

        public void Init(InitAiFactoryData data)
        {
            //nothing
        }

        private class ThirdAiFactoryUnitAi : IBattleUnitAi
        {
            private readonly float _evasion;

            public ThirdAiFactoryUnitAi(float evasion)
            {
                _evasion = evasion;
            }

            public UpdateUnitResult Update(UpdateUnitAiData data)
            {
                UpdateUnitResult result;

                var nearestEnemy = data.Perception.VisibleUnits
                    .Where(unit => !unit.Friendly)
                    .OrderBy(unit => unit.Direction.GetLengthSquare())
                    .FirstOrDefault();
                if (nearestEnemy == null)
                {
                    result.Attack.Enabled = false;
                    result.Attack.Direction = Z.KM.AIBattles.Common.Geometry.Vector2D.Zero;
                    result.Move.Enabled = false;
                    result.Move.Direction = Z.KM.AIBattles.Common.Geometry.Vector2D.Zero;
                    return result;
                }
                if (data.Perception.Energy < data.Perception.ThrowEnergyCost)
                {
                    result.Attack = default(UpdateUnitResult.AttackDesire);
                }
                else
                {
                    result.Attack.Enabled = true;
                    result.Attack.Direction = nearestEnemy.Direction;
                }

                var nearestFriend = data.Perception.VisibleUnits
                    .Where(unit => unit.Friendly)
                    .OrderBy(unit => unit.Direction.GetLengthSquare())
                    .FirstOrDefault();
                result.Move.Enabled = true;
                if (nearestFriend != null)
                {
                    result.Move.Direction = nearestFriend.Direction.Normalize();
                    result.Move.Direction -= nearestEnemy.Direction.Normalize() * _evasion;
                }
                else
                {
                    result.Move.Direction = nearestEnemy.Direction;
                }

                if (data.Perception.VectorToNearestTerrainEdge.GetLengthSquare() < 5)
                {
                    result.Move.Enabled = true;
                    result.Move.Direction += -data.Perception.VectorToNearestTerrainEdge.Normalize(5 - data.Perception.VectorToNearestTerrainEdge.GetLengthSquare()) * 0.3f;
                }

                return result;
            }
        }
    }
}