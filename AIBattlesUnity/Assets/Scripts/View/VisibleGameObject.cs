﻿using UnityEngine;
using Z.KM.AIBattles.Common.Geometry;

namespace Z.KM.AIBattles.Unity.View
{
    public class VisibleGameObject : MonoBehaviour
    {

        [SerializeField]
        private SpriteRenderer _toRecolor;

        private bool _isDead;

        public void Display(Vector2D position, string name, Color color, bool isDead)
        {
            Display(_toRecolor.sprite, position, name, color, isDead);
        }

        public void Display(Sprite sprite, Vector2D position, string name, Color color, bool isDead)
        {
            transform.position = new Vector3(position.X, position.Y);
            _toRecolor.sortingOrder = 1000000 - (int)(position.Y * 100);
            gameObject.name = name;
            _toRecolor.color = color;
            _toRecolor.sprite = sprite;
            _isDead = isDead;
            gameObject.transform.localRotation = Quaternion.Euler(0, 0, _isDead ? -90 : 0);
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.name = "object in pool";
            gameObject.SetActive(false);
            _isDead = true;
        }

        internal bool IsDead()
        {
            return _isDead;
        }
    }
}