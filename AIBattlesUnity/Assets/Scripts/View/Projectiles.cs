﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Z.KM.AIBattles.Common.Core.BattleRepresentation;

namespace Z.KM.AIBattles.Unity.View
{
    class Projectiles
    {
        private readonly VisibleGameObject _projectilePrefab;
        private readonly List<VisibleGameObject> _projectiles;

        public Projectiles(VisibleGameObject projectilePrefab)
        {
            _projectilePrefab = projectilePrefab;
            _projectiles = new List<VisibleGameObject>();
        }

        public void Update(ReadOnlyCollection<IBattleProjectileRepresentation> projectiles)
        {
            for (var i = 0; i < projectiles.Count; i++)
            {
                if (_projectiles.Count <= i)
                {
                    _projectiles.Add(Object.Instantiate(_projectilePrefab));
                }
                var projectileObject = _projectiles[i];
                projectileObject.Display(projectiles[i].Position, "projectile", Color.white, false);
                projectileObject.transform.localScale = new Vector2(projectiles[i].Speed.X >= 0 ? 1 : -1, 1);
            }
            for (var i = projectiles.Count; i < _projectiles.Count; i++)
            {
                _projectiles[i].Hide();
            }
        }
    }
}