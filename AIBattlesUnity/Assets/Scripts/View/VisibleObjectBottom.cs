﻿using UnityEngine;

namespace Z.KM.AIBattles.Unity.View
{
    public class VisibleObjectBottom : MonoBehaviour
    {

        [SerializeField]
        private Transform _radiusTransform;
        [SerializeField]
        private SpriteRenderer _toRecolor;
        [SerializeField]
        private Color _fullHealthColor;
        [SerializeField]
        private Color _noHealthColor;

        public void Hide()
        {
            gameObject.name = "hidden";
            gameObject.SetActive(false);
        }

        public void Display(float radius, float colorLerpValue)
        {
            _radiusTransform.localScale = new Vector3(radius, radius) * 2;
            _toRecolor.color = Color.Lerp(_noHealthColor, _fullHealthColor, colorLerpValue);
            gameObject.SetActive(true);
        }
    }
}
