﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using Z.KM.AIBattles.Common.Core.BattleRepresentation;

namespace Z.KM.AIBattles.Unity.View
{
    public class Units
    {
        private readonly VisibleUnit _unitPrefab;
        private readonly List<VisibleUnit> _units;

        public Units(VisibleUnit unitPrefab)
        {
            _unitPrefab = unitPrefab;
            _units = new List<VisibleUnit>();
        }

        public void Update(ReadOnlyCollection<IBattleUnitRepresentation> units)
        {
            for (var i = 0; i < units.Count; i++)
            {
                if (_units.Count <= i)
                {
                    _units.Add(UnityEngine.Object.Instantiate(_unitPrefab));
                }
                var unit = units[i];
                var unitObject = _units[i];
                unitObject.Display(unit);
            }
            for (var i = units.Count; i < _units.Count; i++)
            {
                _units[i].Hide();
            }
        }

        public Rect GetBounds()
        {
            var aliveUnits = _units.Where(unit => !unit.IsDead()).ToList();
            if (aliveUnits.Count == 0)
            {
                return Rect.zero;
            }
            var firstUnitPosition = To2D(aliveUnits[0].transform.position);
            var min = aliveUnits
                .Select(unit => To2D(unit.transform.position))
                .Aggregate(
                    firstUnitPosition,
                    (aggregatedValue, newValue) => new Vector2(
                        Mathf.Min(aggregatedValue.x, newValue.x),
                        Mathf.Min(aggregatedValue.y, newValue.y)
                    )
                );
            var max = aliveUnits
                .Select(unit => To2D(unit.transform.position))
                .Aggregate(
                    firstUnitPosition,
                    (aggregatedValue, newValue) => new Vector2(
                        Mathf.Max(aggregatedValue.x, newValue.x),
                        Mathf.Max(aggregatedValue.y, newValue.y)
                    )
                );
            Rect result = new Rect(min, max - min);
            return result;
        }

        private static Vector2 To2D(Vector3 source)
        {
            return new Vector2(source.x, source.y);
        }
    }
}