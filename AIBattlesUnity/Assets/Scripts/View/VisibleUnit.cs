﻿using UnityEngine;
using Z.KM.AIBattles.Common.Core.BattleRepresentation;

namespace Z.KM.AIBattles.Unity.View
{
    public class VisibleUnit : MonoBehaviour
    {
        [SerializeField]
        private VisibleGameObject _main;
        [SerializeField]
        private VisibleObjectBottom _bottom;
        [SerializeField]
        private UnitSpritesCollection _spritesCollection;

        private float _lastDirectionChangeTime;
        private bool _lastDirectionWasRight;
        private const float _changeDirectionMinTime = 0.1f;

        internal void Display(IBattleUnitRepresentation unit)
        {
            var healthPercents = (float)unit.Health / unit.MaxHealth;
            bool isAlive = healthPercents > 0;
            const float deadColor = 0.4f;
            _main.Display(_spritesCollection.GetSpriteForTeamId(unit.Team.Id), unit.Position, "Unit " + unit.Team.Id, isAlive ? Color.white : new Color(deadColor, deadColor, deadColor, 0.3f), !isAlive);

            var nowDirectionIsRight = unit.Speed.X >= 0;
            if(_lastDirectionWasRight != nowDirectionIsRight)
            {
                _lastDirectionWasRight = nowDirectionIsRight;
                _lastDirectionChangeTime = Time.time;
            }
            if (Time.time >= _lastDirectionChangeTime + _changeDirectionMinTime)
            {
                _main.transform.localScale = new Vector3(
                    Mathf.Abs(_main.transform.localScale.x) * (_lastDirectionWasRight ? 1 : -1),
                    _main.transform.localScale.y,
                    _main.transform.localScale.z
                );
                _lastDirectionChangeTime = Time.realtimeSinceStartup;
            }
            if (isAlive)
            {
                _bottom.Display(unit.Radius, healthPercents);
            }
            else
            {
                _bottom.Hide();
            }
        }

        internal void Hide()
        {
            _bottom.Hide();
            _main.Hide();
        }

        internal bool IsDead()
        {
            return _main.IsDead();
        }
    }
}
