﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Z.KM.AIBattles.Unity.UI.Tourney
{
    public class TourneyDialogGridCell : MonoBehaviour
    {
        [SerializeField]
        private Text _text;
        [SerializeField]
        private Image _icon;
        [SerializeField]
        private Image _background;

        private Action _onClick;

        public void Init(string text, Sprite icon, Color backgroundColor, Action onClick)
        {
            _onClick = onClick;
            _background.color = backgroundColor;

            if (!string.IsNullOrEmpty(text))
            {
                _text.text = text;
                _text.gameObject.SetActive(true);
            }
            else
            {
                _text.gameObject.SetActive(false);
            }

            if (icon != null)
            {
                _icon.sprite = icon;
                _icon.gameObject.SetActive(true);
            }
            else
            {
                _icon.gameObject.SetActive(false);
            }
        }

        public void OnClick()
        {
            if (_onClick != null)
            {
                _onClick();
            }
        }

    }
}
