﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Z.KM.AIBattles.Unity.Tourney;

namespace Z.KM.AIBattles.Unity.UI.Tourney
{
    public class TourneyDialog : MonoBehaviour
    {
#pragma warning disable 647
        [SerializeField]
        private Text _caption;
        [SerializeField]
        private GridLayoutGroup _grid;
        [SerializeField]
        private RectTransform _gridParent;
        [SerializeField]
        private TourneyDialogGridCell _gridCellPrefab;
#pragma warning restore

        private static readonly Color CellColorSystem = new Color(0, 0, 0, 0.4f);
        private static readonly Color CellColorNotCalculated = new Color(0, 0, 0, 0.2f);
        private static readonly Color CellColorInProgress = new Color(0.5f, 0.5f, 0.5f, 0.2f);
        private static readonly Color CellColorWin = new Color(0, 1, 0, 0.2f);
        private static readonly Color CellColorLose = new Color(1, 0, 0, 0.2f);
        private static readonly Color CellColorDraw = new Color(0.5f, 0.5f, 0.5f, 0.2f);

        private List<TourneyDialogGridCell> _currentCells;
        private ViewModel _model;
        private TourneyMainRepresentation _representation;

        public struct ViewModel
        {
            public TourneyMain Tourney;
            public Action OnRestartClick;
            public UnitSpritesCollection SpritesCollection;
            public Action<ReadOnlyCollection<Team>, TourneyMain.Settings> OnPlayBattleClick;
            public Action<ReadOnlyCollection<Team>, TourneyMain.Settings> OnHustleClick;
        }

        public void ShowTourney(ViewModel viewModel)
        {
            _model = viewModel;
            PrepareTourney();
            Refresh();
        }

        private void PrepareTourney()
        {
            if (_currentCells == null)
            {
                _currentCells = new List<TourneyDialogGridCell>();
            }
            var teamsCount = _model.Tourney.GetTeams().Count;
            _grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
            var columnsCount = teamsCount + 2;
            _grid.constraintCount = columnsCount;
            _grid.cellSize = new Vector2(_gridParent.rect.width / columnsCount, _grid.cellSize.y);

            var cellsCount = (teamsCount + 1) * (teamsCount + 2);
            while (_currentCells.Count > cellsCount)
            {
                Destroy(_currentCells[_currentCells.Count - 1].gameObject);
                _currentCells.RemoveAt(_currentCells.Count - 1);
            }
            while (_currentCells.Count < cellsCount)
            {
                var cell = Instantiate(_gridCellPrefab, _grid.transform, false);
                _currentCells.Add(cell);
            }

            _currentCells[0].Init(null, null, CellColorSystem, null);
            _currentCells[teamsCount + 1].Init("Total Pts", null, CellColorSystem, null);

            _representation = new TourneyMainRepresentation(_model.Tourney);

        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Update()
        {
            if (_model.Tourney == null || _model.Tourney.IsFinished())
            {
                return;
            }
            _model.Tourney.Update();
            Refresh();
        }

        private void Refresh()
        {
            _caption.text = _representation.IsFinished()
                ? "Finished (" + _representation.IterationsBetweenTwoTeamsCount + "x" + _representation.GetSettingsPerTwoTeamCount() + " iterations/settings per pair)"
                : ("Calculating (" + (_representation.GetFinishPercents() * 100).ToString("0.") + "%)");

            var teams = _representation.CreateTeams(_representation.IsFinished());
            var teamsCount = teams.Count;
            var columnsCount = teamsCount + 2;

            //top legend
            for (var i = 0; i < teams.Count; i++)
            {
                var row = teams[i];
                var columnCellIndex = i + 1;
                _currentCells[columnCellIndex].Init(row.Team.Name, _model.SpritesCollection.GetSpriteForTeamId(row.Team.Id), CellColorSystem, null);
            }

            //team rows
            for (var i = 0; i < teams.Count; i++)
            {
                var team = teams[i];

                var rowHeaderIndex = columnsCount * (i + 1);
                _currentCells[rowHeaderIndex].Init(team.Team.Name, _model.SpritesCollection.GetSpriteForTeamId(team.Team.Id), CellColorSystem, null);

                for (var j = 0; j < teams.Count; j++)
                {
                    var otherTeam = teams[j].Team;
                    var battle = _representation.GetBattleBetweenTeams(team.Team, otherTeam);

                    string text;
                    Color color;
                    Action clickAction;
                    switch (battle.State)
                    {
                        case TourneyMainRepresentation.BattleWithAnotherTeam.StateType.SameTeam:
                            text = "-";
                            color = CellColorSystem;
                            clickAction = () =>
                            {
                                ClickCell(team.Team, team.Team.CloneForSelfAttack());
                            };
                            break;
                        case TourneyMainRepresentation.BattleWithAnotherTeam.StateType.NotStarted:
                            text = "...";
                            color = CellColorNotCalculated;
                            clickAction = () =>
                            {
                                ClickCell(team.Team, otherTeam);
                            };
                            break;
                        case TourneyMainRepresentation.BattleWithAnotherTeam.StateType.InProgressNow:
                            text = "VS";
                            color = CellColorInProgress;
                            clickAction = () =>
                            {
                                ClickCell(team.Team, otherTeam);
                            };
                            break;
                        case TourneyMainRepresentation.BattleWithAnotherTeam.StateType.Completed:
                            var winPercentage = battle.WinPercentage;
                            switch (battle.Result)
                            {
                                case TourneyMainRepresentation.BattleWithAnotherTeam.ResultType.Win:
                                    color = CellColorWin;
                                    break;
                                case TourneyMainRepresentation.BattleWithAnotherTeam.ResultType.Draw:
                                    color = CellColorDraw;
                                    break;
                                case TourneyMainRepresentation.BattleWithAnotherTeam.ResultType.Lose:
                                    color = CellColorLose;
                                    break;
                                default: throw new Exception("default switch");
                            }

                            var cellText = new StringBuilder();
                            cellText.Append((100 * winPercentage).ToString("0.")).Append("%");
                            cellText.AppendLine();
                            cellText.Append(battle.WinCount);
                            if (battle.DrawCount != 0)
                            {
                                cellText.Append(" (").Append(battle.DrawCount).Append(" draw)");
                            }
                            text = cellText.ToString();
                            clickAction = () =>
                            {
                                ClickCell(team.Team, otherTeam);
                            };
                            break;
                        default:
                            throw new Exception("TourneyDialog.Refresh default switch");
                    }

                    _currentCells[rowHeaderIndex + j + 1].Init(
                        text,
                        null,
                        color,
                        clickAction
                    );
                }
                _currentCells[rowHeaderIndex + teamsCount + 1].Init(team.GainedScorePerTeam + "/" + team.GainedScorePerBattles, null, CellColorSystem, null);
            }
        }

        private void ClickCell(Team teamA, Team teamB)
        {
            var teamsList = new List<Team>();
            teamsList.Add(teamA);
            teamsList.Add(teamB);
            _model.OnPlayBattleClick(teamsList.AsReadOnly(), _model.Tourney.GetSettings());
        }

        public void OnRestartClick()
        {
            _model.OnRestartClick();
        }

        public void OnHustleClick()
        {
            var teams = _model.Tourney.GetTeams();
            _model.OnHustleClick(teams, _model.Tourney.GetSettings());
        }
    }
}
