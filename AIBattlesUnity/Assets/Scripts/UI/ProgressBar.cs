﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] private Text _label;

    [SerializeField] private RectTransform _lineBar;

    [SerializeField] private RectTransform _backBar;

    [SerializeField] private Image _icon;
 

    public void SetProgress(float percent)
    {
        percent = Mathf.Clamp(percent, 0, 1);
        _lineBar.anchorMax = new Vector2(percent, _lineBar.anchorMax.y);
    }

    public void SetLabel(string text)
    {
        _label.text = text;
    }

    public void SetIconTeam(Sprite sprite)
    {
        _icon.sprite = sprite;
    }
}