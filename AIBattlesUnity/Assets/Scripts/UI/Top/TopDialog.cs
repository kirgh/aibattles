﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using Z.KM.AIBattles.Common.Core.BattleRepresentation;
using Z.KM.AIBattles.Unity;

/// <summary>
/// Окно статистики боя
/// </summary>
public class TopDialog : MonoBehaviour
{
    [SerializeField]
    private RectTransform _progressBarsContainer;

    [SerializeField]
    private ProgressBar _progressBarPrefab;

    [SerializeField]
    private UnitSpritesCollection _spritesCollection;

    private Dictionary<int, ProgressBar> _progressBars;
    private Action _onBackClick;

    protected void Awake()
    {
        _progressBars = new Dictionary<int, ProgressBar>();
    }

    public void OnBattleFinish(IBattleRepresentation battleRepresentation)
    {
        UpdateAllProgressBars(battleRepresentation);
    }

    public void OnBattleUpdate(IBattleRepresentation battleRepresentation)
    {
        UpdateAllProgressBars(battleRepresentation);
    }

    private void UpdateAllProgressBars(IBattleRepresentation battleRepresentation)
    {
        var teamIds = battleRepresentation.GetUnits().Select(s => s.Team.Id).Distinct().ToList();
        foreach (var id in teamIds)
        {
            var units = battleRepresentation.GetUnits().Where(u => u.Team.Id == id);
            var health = units.Sum(h => h.Health);
            var maxHealth = units.Sum(h => h.MaxHealth);
            float value = (float)health / maxHealth;
            RefreshProgressBar(id, value);
        }
    }

    public void OnBattleStart(IBattleRepresentation battleRepresentation, ReadOnlyCollection<Team> teams)
    {
        foreach (Transform child in _progressBarsContainer)
        {
            Destroy(child.gameObject);
        }

        _progressBars.Clear();

        var teamIds = battleRepresentation.GetUnits().Select(s => s.Team.Id).Distinct().ToList();
        foreach (var id in teamIds)
        {
            var progressBar = Instantiate(_progressBarPrefab, _progressBarsContainer, false);
            progressBar.SetLabel(teams.FirstOrDefault(team => team.Id == id).Name);
            progressBar.SetProgress(1.0f);
            progressBar.SetIconTeam(_spritesCollection.GetSpriteForTeamId(id));
            progressBar.gameObject.SetActive(true);
            _progressBars.Add(id, progressBar);
        }
    }

    internal void Init(Action onBackClick)
    {
        _onBackClick = onBackClick;
    }

    private void RefreshProgressBar(int teamId, float newValue)
    {
        if (_progressBars == null || !_progressBars.ContainsKey(teamId))
        {
            return;
        }

        _progressBars[teamId].SetProgress(newValue);
        _progressBars[teamId].SetIconTeam(_spritesCollection.GetSpriteForTeamId(teamId));
    }

    public void OnBackClick()
    {
        _onBackClick();
    }
}