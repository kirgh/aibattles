﻿using Z.KM.AIBattles.Common.Core;

namespace Z.KM.AIBattles.Core
{
    public static class AIBattlesDefaultFactory
    {
        public static IBattle CreateBattle(CreateBattleSettings settings)
        {
            return new Battle(settings);
        }
    }
}
