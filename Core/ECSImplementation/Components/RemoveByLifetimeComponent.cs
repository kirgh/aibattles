﻿using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    internal class RemoveByLifetimeComponent : IComponent
    {
        public readonly float LifeTime;

        public RemoveByLifetimeComponent(float lifeTime)
        {
            LifeTime = lifeTime;
        }

    }
}
