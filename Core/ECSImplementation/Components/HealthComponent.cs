﻿using System;
using KM.Log;
using Z.KM.AIBattles.Common.AI;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// компонент, дающий возможность жить и иметь здоровье
    /// TODO: это два разные компонента с другими названиями и функциями.
    /// </summary>
    internal class HealthComponent : IComponent
    {

        private static readonly int maxEnumLength;
        private HealthPerception _asPerception;

        public readonly int MaxHealth;
        public int Health { get; private set; }
        public bool IsAlive { get; private set; }

        static HealthComponent()
        {
            maxEnumLength = Enum.GetValues(typeof(HealthPerception)).Length;
        }

        public HealthComponent(int maxHealth)
        {
            Health = maxHealth;
            MaxHealth = maxHealth;
            IsAlive = Health > 0;
            _asPerception = GetPerception();
        }

        public void Hit(int damage)
        {
            if (!IsAlive)
            {
                Logger.Error("Do not hit dead!!!");
            }
            Health -= damage;
            IsAlive = Health > 0;
            _asPerception = GetPerception();
        }

        public HealthPerception AsPerception()
        {
            return _asPerception;
        }

        private HealthPerception GetPerception()
        {
            Logger.TodoLowPriority("и это так себе (код и сам метод)");
            if (Health >= MaxHealth)
            {
                return HealthPerception.Healthy;
            }
            if(Health <= 0)
            {
                return HealthPerception.Critical;
            }
            return (HealthPerception)((float) Health * (maxEnumLength - 1) / MaxHealth);
        }
    }
}
