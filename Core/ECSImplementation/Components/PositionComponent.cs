﻿using Z.KM.AIBattles.Common.Geometry;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// компонент, позволяющий находиться где-либо
    /// </summary>
    internal class PositionComponent : IComponent
    {
        public PositionComponent(Vector2D position)
        {
            Position = position;
        }

        public Vector2D Position { get; set; }
    }
}
