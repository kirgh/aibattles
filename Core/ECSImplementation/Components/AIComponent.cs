﻿using Z.KM.AIBattles.Common.AI;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// компонент, подключающий AI
    /// </summary>
    internal class AIComponent : IComponent
    {
        public readonly IBattleUnitAi AI;

        public AIComponent(IBattleUnitAi ai)
        {
            AI = ai;
        }
    }
}
