﻿using Z.KM.AIBattles.Common.Geometry;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// комопнент, имитирующий физику движения через набор сил, действующих на объект.
    /// каждый кадр системы ожидается Reset, что не факт, что правильно.
    /// </summary>
    internal class SelfMovableComponent : IComponent
    {
        public readonly float Deceleration;
        public readonly float Acceleration;
        private readonly float _maxSpeed;

        public Vector2D DesiredSpeed { private set; get; }

        public SelfMovableComponent(float deceleration, float acceleration, float maxSpeed)
        {
            Deceleration = deceleration;
            _maxSpeed = maxSpeed;
            Acceleration = acceleration;
        }

        /// <summary>
        /// 1 => MaxSpeed
        /// </summary>
        public void SetDesiredSpeed(Vector2D value)
        {
            DesiredSpeed = value.LimitMagnitude(1) * _maxSpeed;
        }

        public void Reset()
        {
            DesiredSpeed = Vector2D.Zero;
        }
    }
}
