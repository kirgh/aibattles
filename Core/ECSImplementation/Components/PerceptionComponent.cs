﻿using System.Collections.ObjectModel;
using Z.KM.AIBattles.Common.AI;
using Z.KM.AIBattles.Common.Geometry;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// компонент, дающий возможность видеть/чувствовать
    /// </summary>
    internal class PerceptionComponent : IComponent
    {
        public ReadOnlyCollection<IPerceptionVisibleUnit> VisibleUnits;
        public ReadOnlyCollection<IPerceptionVisibleProjectile> VisibleProjectiles;
        public float IslandRadius;
        public Vector2D VectorToNearestEdge;
    }
}
