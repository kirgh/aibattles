﻿using KM.Log;
using Z.KM.AIBattles.Common.Core;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    internal class EnergyComponent : IComponent
    {
        public readonly float MaxValue;
        public readonly float IncreaseSpeed;
        public float Current { private set; get; }

        public EnergyComponent(CreateBattleSettings.UnitEnergySetttings energy)
        {
            Current = energy.MaxEnergy;
            MaxValue = energy.MaxEnergy;
            IncreaseSpeed = energy.IncreaseSpeed;
        }

        internal void Delta(float value)
        {

            Current += value;
            if(Current < 0)
            {
                Logger.Error("EnergyComponent.Delta less than zero");
                Current = 0;
            }
            Current = Current > MaxValue ? MaxValue : Current;
        }
    }
}
