﻿using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// компонент, описывающий физическое тело (сейчас это шар или сфера)
    /// </summary>
    internal class BodyComponent : IComponent
    {
        public readonly float Radius;

        public BodyComponent(float radius)
        {
            Radius = radius;
        }
    }
}
