﻿using Z.KM.AIBattles.Common.Core.BattleRepresentation;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// компонент, обозначающий, что это юнит.
    /// TODO: это неправильно, но нет времени и не совсем понятно, как быть.
    /// </summary>
    internal class BattleUnitComponent : IComponent
    {
        public readonly IBattleTeamRepresentation Team;
        public readonly int UniqueIdInTeam;

        public BattleUnitComponent(IBattleTeamRepresentation team, int uniqueIdInTeam)
        {
            Team = team;
            UniqueIdInTeam = uniqueIdInTeam;
        }

        internal bool IsFriendly(BattleUnitComponent other)
        {
            return Team.IsFriendly(other.Team);
        }
    }
}
