﻿using Z.KM.AIBattles.Common.Geometry;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// компонент "земли".
    /// на самом деле сейчас это скорее "круглый постепенно уменьшающийся остров",
    /// причём теоретически их может быть несколько, но не все это поддерживают.
    /// </summary>
    internal class TerrainComponent : IComponent
    {
        public readonly Vector2D Position;
        public readonly float StartRadius;
        public readonly float StartShrinkTime;
        public readonly float EndShrinkTime;

        public float Radius;

        public TerrainComponent(Vector2D position, float radius, float startShrinkTime, float endShrinkTime)
        {
            Position = position;
            StartRadius = Radius = radius;
            StartShrinkTime = startShrinkTime;
            EndShrinkTime = endShrinkTime;
        }
    }
}
