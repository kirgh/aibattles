﻿using Z.KM.AIBattles.Common.Geometry;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// компонент, добавляющий возможность кастовать/атаковать.
    /// </summary>
    internal class CasterComponent : IComponent
    {
        public readonly int Damage;
        public readonly float ProjectileSpeed;
        public readonly float ProjectilePushTargetCoefficient;
        public bool IsTryingToCast { get; set; }
        public Vector2D CastingDirection { get; set; }
        public readonly float CastEnergyCost;
        public readonly float ProjectileLifeTime;

        public CasterComponent(int damage, float projectileSpeed, float castCost, float projectileLifeTime, float projectilePushTargetCoefficient)
        {
            Damage = damage;
            ProjectileSpeed = projectileSpeed;
            CastEnergyCost = castCost;
            ProjectileLifeTime = projectileLifeTime;
            ProjectilePushTargetCoefficient = projectilePushTargetCoefficient;
        }

        internal void DoNotCast()
        {
            CastingDirection = Vector2D.Zero;
            IsTryingToCast = false;
        }
    }
}
