﻿using KM.Log;
using Z.KM.AIBattles.Common.Geometry;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// компонент, двигающийся куда-нибудь с линейной скоростью
    /// </summary>
    internal class MovableComponent : IComponent
    {

        public Vector2D Speed;

        public MovableComponent(Vector2D initialSpeed)
        {
            Speed = initialSpeed;
        }

        public Vector2D LookDirection
        {
            get
            {
                Logger.TodoLowPriority("LookDirection is fake");
                if (Speed.Equals(Vector2D.Zero))
                {
                    return Vector2D.AxisX;
                }
                return Speed;
            }
        }
    }
}
