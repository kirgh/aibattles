﻿using KM.Log;
using Z.KM.AIBattles.Common.Geometry;
using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    internal class ProjectileComponent : IComponent
    {
        public readonly int Damage;
        public readonly float PushTargetSpeedCoefficient;

        private ProjectileComponent(int damage, float pushTargetSpeedCoefficient)
        {
            Damage = damage;
            PushTargetSpeedCoefficient = pushTargetSpeedCoefficient;
        }

        public static GameObject CreateGameObject(int damage, Vector2D position, Vector2D direction, float speed, float lifeTime, float pushTargetSpeedCoefficient)
        {
            Logger.TodoLowPriority("private и static - это я так, побаловаться, может, удалить");
            var result = new GameObject();
            result.AddComponent(new PositionComponent(position));
            var movable = new MovableComponent(direction.Normalize() * speed);
            result.AddComponent(movable);
            result.AddComponent(new ProjectileComponent(damage, pushTargetSpeedCoefficient));
            result.AddComponent(new RemoveByLifetimeComponent(lifeTime));
            result.AddComponent(new TimeComponent());
            return result;
        }
    }
}
