﻿using Z.KM.ECS;

namespace Z.KM.AIBattles.Core.ECSImplementation.Components
{
    /// <summary>
    /// компонент, хранящий время
    /// </summary>
    internal class TimeComponent : IComponent
    {
        public float TimeFromStart = 0;
    }
}
