﻿using KM.Log;
using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{

    internal class CastingSystem : AbstractSingleNodeSystem<CastingSystemNode>
    {
        private readonly IObjectsContainer _objectsContainer;

        public CastingSystem(IObjectsContainer objectsContainer)
        {
            _objectsContainer = objectsContainer;
        }

        protected override bool ConvertToNode(IComponentHolder gameObject, out CastingSystemNode result)
        {
            result = new CastingSystemNode();
            result.Caster = gameObject.GetComponent<CasterComponent>();
            result.Energy = gameObject.GetComponent<EnergyComponent>();
            result.Position = gameObject.GetComponent<PositionComponent>();
            result.Body = gameObject.GetComponent<BodyComponent>();
            return result.Caster != null && result.Position != null && result.Energy != null && result.Body != null;
        }

        protected override void UpdateObject(CastingSystemNode node)
        {
            if (!node.Caster.IsTryingToCast)
            {
                return;
            }
            if (node.Energy.Current < node.Caster.CastEnergyCost)
            {
                Logger.Error("CastingSystem.Update trying to spend more energy than exists");
                node.Energy.Delta(-node.Energy.Current);
                return;
            }

            node.Energy.Delta(-node.Caster.CastEnergyCost);

            _objectsContainer.AddObject(
                ProjectileComponent.CreateGameObject(
                    node.Caster.Damage,
                    node.Position.Position + node.Caster.CastingDirection.Normalize() * node.Body.Radius * 1.1f,
                    node.Caster.CastingDirection,
                    node.Caster.ProjectileSpeed,
                    node.Caster.ProjectileLifeTime,
                    node.Caster.ProjectilePushTargetCoefficient
                )
            );

        }
    }

    internal class CastingSystemNode
    {
        public CasterComponent Caster;
        public EnergyComponent Energy;
        public PositionComponent Position;
        public BodyComponent Body;
    }
}

//ДАЛЬШЕ лежит исходный код, это что-то типа MeleeAttackSystem, а можно и просто удалить
/*
{

using KM.Log;
using Z.KM.AIBattles.Common.Geometry;
using Z.KM.AIBattles.Core.ECSCore;
using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.AIBattles.Core.ECSImplementation.Systems.Base;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// система, отвечающая за совершение действия каста/атаки
    /// TODO: конечно, и она на полную переработку
    /// </summary>
    internal class CastingSystem : AbstractNodeToNodeSystem<AttackSystemNode>
    {
        protected override bool ConvertToNode(IComponentHolder gameObject, out AttackSystemNode result)
        {
            result = new AttackSystemNode();
            result.Health = gameObject.GetComponent<HealthComponent>();
            result.Caster = gameObject.GetComponent<CasterComponent>();
            result.Movable = gameObject.GetComponent<MovableComponent>();
            result.Body = gameObject.GetComponent<BodyComponent>();
            return result.Health != null
                && result.Caster != null
                && result.Movable != null
                && result.Body != null;
        }

        protected override void UpdateObjects(AttackSystemNode a, AttackSystemNode b)
        {
            Logger.TodoLowPriority("all is bad and todo here");
            if (!a.Health.IsAlive || !b.Health.IsAlive)
            {
                return;
            }
            if (IsUnderAttack(a, b))
            {
                b.Health.Hit(a.Caster.Damage);
            }
            if (IsUnderAttack(b, a))
            {
                a.Health.Hit(b.Caster.Damage);
            }
        }

        private bool IsUnderAttack(AttackSystemNode unit, AttackSystemNode target)
        {
            if (!unit.Caster.IsCasting)
            {
                return false;
            }
            return IsInLineOfAttack(unit.Movable.Position, target.Movable.Position, unit.Caster.CastingDirection, unit.Caster.Range + unit.Body.Radius + target.Body.Radius);
        }

        private static bool IsInLineOfAttack(Vector2D source, Vector2D target, Vector2D direction, float attackRange)
        {
            Logger.TodoLowPriority("IsInLineOfAttack");
            var toTarget = target - source;
            var lengthSquare = toTarget.GetLengthSquare();
            return lengthSquare == 0 || (lengthSquare <= attackRange * attackRange && toTarget.Dot(direction) > 0);
        }
    }

    internal class AttackSystemNode
    {
        public HealthComponent Health;
        public CasterComponent Caster;
        public MovableComponent Movable;
        public BodyComponent Body;
    }
}
*/
