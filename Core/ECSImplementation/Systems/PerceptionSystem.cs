﻿using KM.Log;
using System.Collections.ObjectModel;
using Z.KM.AIBattles.Common.AI;
using Z.KM.AIBattles.Common.Geometry;
using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.AIBattles.Core.Utils;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{

    /// <summary>
    /// система, отвечающая за обновление ощущений ощущающих
    /// </summary>
    internal class PerceptionSystem : ISystem
    {

        private readonly ObjectToNodeFilter<UnitNode> _units;
        private readonly ObjectToNodeFilter<ProjectileNode> _projectiles;
        private readonly TerrainComponent _terrain;

        public PerceptionSystem(TerrainComponent terrain)
        {
            _units = new ObjectToNodeFilter<UnitNode>(new UnitNodeCreator());
            _projectiles = new ObjectToNodeFilter<ProjectileNode>(new ProjectileNodeCreator());
            _terrain = terrain;
        }

        public static void InitUnitComponents(GameObject unit)
        {
            Logger.TodoLowPriority("чехарда");
            unit.AddComponent(new PerceptionComponent());
            unit.AddComponent(new PerceptionSystemPrivateComponent());
        }

        public void AddObject(IComponentHolder gameObject)
        {
            _units.AddObject(gameObject);
            _projectiles.AddObject(gameObject);
        }

        public void RemoveObject(IComponentHolder gameObject)
        {
            _units.RemoveObject(gameObject);
            bool isProjectile = _projectiles.RemoveObject(gameObject);
            if (isProjectile)
            {
                for (var unitIndex = 0; unitIndex < _units.Output.Count; unitIndex++)
                {
                    _units.Output[unitIndex].PrivatePerception.RemoveProjectile(gameObject);
                }
            }
        }

        public void Update()
        {
            for (var unitIndex = 0; unitIndex < _units.Output.Count; unitIndex++)
            {
                for (var targetUnitIndex = unitIndex + 1; targetUnitIndex < _units.Output.Count; targetUnitIndex++)
                {
                    UpdateUnitToUnitPerception(_units.Output[unitIndex], _units.Output[targetUnitIndex]);
                    UpdateUnitToUnitPerception(_units.Output[targetUnitIndex], _units.Output[unitIndex]);
                }
                for (var projectileIndex = 0; projectileIndex < _projectiles.Output.Count; projectileIndex++)
                {
                    UpdateUnitToProjectilePerception(_units.Output[unitIndex], _projectiles.Output[projectileIndex]);
                }
            }
        }

        private void UpdateUnitToProjectilePerception(UnitNode unit, ProjectileNode projectile)
        {
            Logger.TodoLowPriority("copypast");
            var direction = (projectile.Position.Position - unit.Position.Position).ToAnotherSystem(unit.Movable.LookDirection);
            var speed = projectile.Movable.Speed.ToAnotherSystem(unit.Movable.LookDirection);
            unit.PrivatePerception.UpdateOrCreateVisibleProjectile(projectile.GameObject, direction, speed);
        }

        private void UpdateUnitToUnitPerception(UnitNode unit, UnitNode target)
        {
            if (!unit.Health.IsAlive)
            {
                return;
            }

            if (target.Health.IsAlive)
            {
                var direction = (target.Position.Position - unit.Position.Position).ToAnotherSystem(unit.Movable.LookDirection);
                var speed = target.Movable.Speed.ToAnotherSystem(unit.Movable.LookDirection);
                unit.PrivatePerception.UpdateOrCreateVisibleUnit(
                    target.GameObject,
                    unit.Unit.Team.IsFriendly(target.Unit.Team),
                    target.Body.Radius,
                    target.Health.AsPerception(),
                    direction,
                    speed,
                    target.Unit.Team.Id,
                    target.Unit.UniqueIdInTeam
                );
            }
            else
            {
                unit.PrivatePerception.RemoveUnit(target.GameObject);
            }
            unit.Perception.VisibleUnits = unit.PrivatePerception.VisbileUnits;
            unit.Perception.VisibleProjectiles = unit.PrivatePerception.VisbileProjectiles;
            unit.Perception.IslandRadius = _terrain.Radius;
            if (unit.Position.Position.Equals(_terrain.Position))
            {
                unit.Perception.VectorToNearestEdge = (Vector2D.AxisX * _terrain.Radius).ToAnotherSystem(unit.Movable.LookDirection);
            }
            else
            {
                var d = unit.Position.Position - _terrain.Position;
                unit.Perception.VectorToNearestEdge = d.Normalize(_terrain.Radius - d.GetLength()).ToAnotherSystem(unit.Movable.LookDirection);
            }
        }

        private struct UnitNode
        {
            public IComponentHolder GameObject;
            public HealthComponent Health;
            public PerceptionComponent Perception;
            public PerceptionSystemPrivateComponent PrivatePerception;
            public BodyComponent Body;
            public CasterComponent Caster;
            public BattleUnitComponent Unit;
            public PositionComponent Position;
            public MovableComponent Movable;
        }

        private struct ProjectileNode
        {
            public IComponentHolder GameObject;
            public PositionComponent Position;
            public MovableComponent Movable;
            public ProjectileComponent Projectile;
        }

        private class PerceptionSystemPrivateComponent : IComponent
        {

            private readonly DictionaryToReadOnlyCollection<IComponentHolder, PerceptionVisibleUnit, IPerceptionVisibleUnit> _units;
            private readonly DictionaryToReadOnlyCollection<IComponentHolder, PerceptionVisibleProjectile, IPerceptionVisibleProjectile> _projectiles;

            public PerceptionSystemPrivateComponent()
            {
                _units = new DictionaryToReadOnlyCollection<IComponentHolder, PerceptionVisibleUnit, IPerceptionVisibleUnit>();
                _projectiles = new DictionaryToReadOnlyCollection<IComponentHolder, PerceptionVisibleProjectile, IPerceptionVisibleProjectile>();
            }

            public ReadOnlyCollection<IPerceptionVisibleUnit> VisbileUnits
            {
                get
                {
                    return _units.Output;
                }
            }

            public ReadOnlyCollection<IPerceptionVisibleProjectile> VisbileProjectiles
            {
                get
                {
                    return _projectiles.Output;
                }
            }

            public void RemoveUnit(IComponentHolder id)
            {
                _units.Remove(id);
            }

            public void RemoveProjectile(IComponentHolder id)
            {
                _projectiles.Remove(id);
            }

            public void UpdateOrCreateVisibleUnit(IComponentHolder id, bool friendly, float radius, HealthPerception health, Vector2D direction, Vector2D speed, int teamId, int uniqueIdInTeam)
            {
                var record = _units.GetOrCreate(id);
                record.Friendly = friendly;
                record.Radius = radius;
                record.Health = health;
                record.Direction = direction;
                record.Speed = speed;
                record.TeamId = teamId;
                record.UniqueIdInTeam = uniqueIdInTeam;
            }

            public void UpdateOrCreateVisibleProjectile(IComponentHolder id, Vector2D direction, Vector2D speed)
            {
                var record = _projectiles.GetOrCreate(id);
                record.Direction = direction;
                record.Speed = speed;
            }

            private class PerceptionVisibleUnit : IPerceptionVisibleUnit
            {
                public bool Friendly { get; set; }
                public float Radius { get; set; }
                public HealthPerception Health { get; set; }
                public Vector2D Direction { get; set; }
                public Vector2D Speed { get; set; }
                public int TeamId { get; set; }
                public int UniqueIdInTeam { get; set; }
            }

            private class PerceptionVisibleProjectile : IPerceptionVisibleProjectile
            {
                public Vector2D Direction { get; set; }
                public Vector2D Speed { get; set; }
            }
        }

        private class UnitNodeCreator : ObjectToNodeFilter<UnitNode>.INodeCreator
        {
            public bool ConvertToNode(IComponentHolder gameObject, out UnitNode result)
            {
                result.GameObject = gameObject;
                result.Health = gameObject.GetComponent<HealthComponent>();
                result.Perception = gameObject.GetComponent<PerceptionComponent>();
                result.Caster = gameObject.GetComponent<CasterComponent>();
                result.Body = gameObject.GetComponent<BodyComponent>();
                result.Unit = gameObject.GetComponent<BattleUnitComponent>();
                result.Position = gameObject.GetComponent<PositionComponent>();
                result.Movable = gameObject.GetComponent<MovableComponent>();
                result.PrivatePerception = gameObject.GetComponent<PerceptionSystemPrivateComponent>();
                return result.Health != null
                    && result.Perception != null
                    && result.Caster != null
                    && result.Body != null
                    && result.Unit != null
                    && result.Position != null
                    && result.Movable != null
                    && result.PrivatePerception != null;
            }
        }

        private class ProjectileNodeCreator : ObjectToNodeFilter<ProjectileNode>.INodeCreator
        {
            public bool ConvertToNode(IComponentHolder gameObject, out ProjectileNode result)
            {
                result.GameObject = gameObject;
                result.Position = gameObject.GetComponent<PositionComponent>();
                result.Movable = gameObject.GetComponent<MovableComponent>();
                result.Projectile = gameObject.GetComponent<ProjectileComponent>();
                return result.Projectile != null
                    && result.Position != null
                    && result.Movable != null;
            }
        }

    }
}
