﻿using KM.Log;
using System;
using Z.KM.AIBattles.Common.AI;
using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// система, подключающая AI
    /// </summary>
    class AISystem : AbstractSingleNodeSystem<AISystemNode>
    {

        private readonly TimeComponent _worldTime;
        private readonly TerrainComponent _terrain;

        public AISystem(TimeComponent worldTime, TerrainComponent terrain)
        {
            _worldTime = worldTime;
            _terrain = terrain;
        }

        protected override bool ConvertToNode(IComponentHolder gameObject, out AISystemNode result)
        {
            result.Position = gameObject.GetComponent<PositionComponent>();
            result.Movable = gameObject.GetComponent<MovableComponent>();
            result.AI = gameObject.GetComponent<AIComponent>();
            result.Health = gameObject.GetComponent<HealthComponent>();
            result.Caster = gameObject.GetComponent<CasterComponent>();
            result.Perception = gameObject.GetComponent<PerceptionComponent>();
            result.Body = gameObject.GetComponent<BodyComponent>();
            result.Energy = gameObject.GetComponent<EnergyComponent>();
            result.SelfMovable = gameObject.GetComponent<SelfMovableComponent>();
            result.Unit = gameObject.GetComponent<BattleUnitComponent>();
            return result.Position != null
                && result.Movable != null
                && result.AI != null
                && result.Health != null
                && result.Caster != null
                && result.Perception != null
                && result.Body != null
                && result.SelfMovable != null
                && result.Unit != null;
        }

        protected override void UpdateObject(AISystemNode node)
        {
            if (!node.Health.IsAlive)
            {
                node.Caster.DoNotCast();
                return;
            }

            UpdateUnitAiData updateData;
            updateData.Perception.Radius = node.Body.Radius;
            updateData.Perception.VisibleUnits = node.Perception.VisibleUnits;
            updateData.Perception.Energy = node.Energy.Current;
            updateData.Perception.MaxEnergy = node.Energy.MaxValue;
            updateData.Perception.Health = node.Health.AsPerception();
            updateData.Perception.ThrowEnergyCost = node.Caster.CastEnergyCost;
            updateData.Perception.VisibleProjectiles = node.Perception.VisibleProjectiles;
            updateData.Perception.TerrainRadius = node.Perception.IslandRadius;
            updateData.Perception.VectorToNearestTerrainEdge = node.Perception.VectorToNearestEdge;
            updateData.Perception.TimeFromStart = _worldTime.TimeFromStart;
            updateData.Perception.IsTerrainShrinking = _worldTime.TimeFromStart >= _terrain.StartShrinkTime;
            updateData.Perception.CurrentSpeed = node.Movable.Speed.ToAnotherSystem(node.Movable.LookDirection);
            updateData.UniqueIdInTeam = node.Unit.UniqueIdInTeam;
            UpdateUnitResult updateResult;
            try
            {
                updateResult = node.AI.AI.Update(updateData);
            }
            catch (Exception e)
            {
                Logger.Error("AISystem.UpdateObject exception: " + e.ToString());
                node.Health.Hit(node.Health.Health);
                node.Caster.DoNotCast();
                return;
            }
            if (updateResult.Move.Enabled)
            {
                node.SelfMovable.SetDesiredSpeed(
                    updateResult.Move.Direction.FromAnotherSystem(node.Movable.LookDirection)
                    );
            }

            if (updateResult.Attack.Enabled)
            {
                if (node.Energy.Current >= node.Caster.CastEnergyCost)
                {
                    node.Caster.CastingDirection = updateResult.Attack.Direction.FromAnotherSystem(node.Movable.LookDirection);
                    node.Caster.IsTryingToCast = true;
                }
                else
                {
                    Logger.Error("AISystem.UpdateObject trying to spend more energy than exists");
                    node.Health.Hit(node.Health.Health);
                    node.Caster.DoNotCast();
                }
            }
            else
            {
                node.Caster.DoNotCast();
            }
        }
    }

    struct AISystemNode
    {
        public PositionComponent Position;
        public MovableComponent Movable;
        public BodyComponent Body;
        public AIComponent AI;
        public HealthComponent Health;
        public CasterComponent Caster;
        public EnergyComponent Energy;
        public PerceptionComponent Perception;
        public SelfMovableComponent SelfMovable;
        public BattleUnitComponent Unit;
    }
}
