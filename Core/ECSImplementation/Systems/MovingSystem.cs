﻿using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// система, которая сейчас отвечает за кинематическое движение живых объектов
    /// TODO: криво
    /// </summary>
    internal class MovingSystem : AbstractSingleNodeSystem<MovingSystemNode>
    {
        private readonly TimeSystem _time;

        public MovingSystem(TimeSystem time)
        {
            _time = time;
        }

        protected override bool ConvertToNode(IComponentHolder gameObject, out MovingSystemNode result)
        {
            result.Movable = gameObject.GetComponent<MovableComponent>();
            result.Position = gameObject.GetComponent<PositionComponent>();
            return result.Movable != null && result.Movable != null;
        }

        protected override void UpdateObject(MovingSystemNode node)
        {
            node.Position.Position = node.Position.Position + node.Movable.Speed * _time.UpdateInterval;
        }
    }

    internal struct MovingSystemNode
    {
        public PositionComponent Position;
        public MovableComponent Movable;
    }
}
