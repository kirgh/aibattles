﻿using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// система для работы со временем
    /// </summary>
    internal class TimeSystem : AbstractSingleNodeSystem<TimeComponent>
    {

        public readonly float UpdateInterval;

        public TimeSystem(float updateInterval)
        {
            UpdateInterval = updateInterval;
        }

        protected override bool ConvertToNode(IComponentHolder gameObject, out TimeComponent result)
        {
            result = gameObject.GetComponent<TimeComponent>();
            return result != null;
        }

        protected override void UpdateObject(TimeComponent node)
        {
            node.TimeFromStart += UpdateInterval;
        }
    }
}
