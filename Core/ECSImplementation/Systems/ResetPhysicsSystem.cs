﻿using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// техническая система, сбрасывающая состояние сил, действующих на движущиеся объекты.
    /// </summary>
    internal class ResetPhysicsSystem : ISystem
    {
        private ResetForcesSystem _subSystem = new ResetForcesSystem();

        public void AddObject(IComponentHolder gameObject)
        {
            _subSystem.AddObject(gameObject);
        }

        public void RemoveObject(IComponentHolder gameObject)
        {
            _subSystem.RemoveObject(gameObject);
        }

        public void Update()
        {
            _subSystem.Update();
        }

        private class ResetForcesSystem : AbstractSingleNodeSystem<SelfMovableComponent>
        {
            protected override bool ConvertToNode(IComponentHolder gameObject, out SelfMovableComponent result)
            {
                result = gameObject.GetComponent<SelfMovableComponent>();
                return result != null;
            }

            protected override void UpdateObject(SelfMovableComponent node)
            {
                node.Reset();
            }
        }
    }
}
