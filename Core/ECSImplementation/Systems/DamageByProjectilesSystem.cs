﻿using KM.Log;
using System;
using Z.KM.AIBattles.Common.Geometry;
using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    internal class DamageByProjectilesSystem : ISystem
    {

        private readonly ObjectToNodeFilter<Target> _targets = new ObjectToNodeFilter<Target>(new Target.Creator());
        private readonly ObjectToNodeFilter<Projectile> _projectiles = new ObjectToNodeFilter<Projectile>(new Projectile.Creator());
        private readonly float _updateInterval;
        private readonly IObjectsContainer _objectsContainer;

        public DamageByProjectilesSystem(IObjectsContainer objectsContainer, float updateInterval)
        {
            Logger.TodoLowPriority("Hit наперёд считается, а надо как системы расположены и вообще CollideSystem");
            _updateInterval = updateInterval;
            _objectsContainer = objectsContainer;
        }

        public void AddObject(IComponentHolder gameObject)
        {
            _targets.AddObject(gameObject);
            _projectiles.AddObject(gameObject);
        }

        public void RemoveObject(IComponentHolder gameObject)
        {
            _targets.RemoveObject(gameObject);
            _projectiles.RemoveObject(gameObject);
        }

        public void Update()
        {
            for (var projectileIndex = 0; projectileIndex < _projectiles.Output.Count; projectileIndex++)
            {
                var projectile = _projectiles.Output[projectileIndex];
                var hit = false;
                for (var targetIndex = 0; targetIndex < _targets.Output.Count; targetIndex++)
                {
                    var target = _targets.Output[targetIndex];
                    if (!target.Health.IsAlive)
                    {
                        continue;
                    }

                    if (!Hit(target, projectile))
                    {
                        continue;
                    }

                    target.Health.Hit(projectile.ProjectileComponent.Damage);
                    target.Movable.Speed += projectile.Movable.Speed * projectile.ProjectileComponent.PushTargetSpeedCoefficient;
                    hit = true;
                }
                if (hit)
                {
                    _objectsContainer.RemoveObject(projectile.GameObject);
                    projectileIndex--;
                }
            }
        }

        private bool Hit(Target target, Projectile projectile)
        {
            return SegmentIntersectsCircle(projectile.Position.Position, projectile.Position.Position + projectile.Movable.Speed * _updateInterval, target.Position.Position, target.Body.Radius);
        }

        /// <summary>
        /// https://stackoverflow.com/questions/1073336/circle-line-segment-collision-detection-algorithm
        /// </summary>
        private static bool SegmentIntersectsCircle(Vector2D E, Vector2D L, Vector2D C, float r)
        {

            var d = L - E; // (Direction vector of ray, from start to end )
            var f = E - C; // (Vector from center sphere to ray start)
            float a = d.Dot(d);
            float b = 2 * f.Dot(d);
            float c = f.Dot(f) - r * r;

            float discriminant = b * b - 4 * a * c;
            if (discriminant < 0)
            {
                return false;
            }
            // ray didn't totally miss sphere,
            // so there is a solution to
            // the equation.

            discriminant = (float)Math.Sqrt(discriminant);

            // either solution may be on or off the ray so need to test both
            // t1 is always the smaller value, because BOTH discriminant and
            // a are nonnegative.
            float t1 = (-b - discriminant) / (2 * a);
            float t2 = (-b + discriminant) / (2 * a);

            // 3x HIT cases:
            //          -o->             --|-->  |            |  --|->
            // Impale(t1 hit,t2 hit), Poke(t1 hit,t2>1), ExitWound(t1<0, t2 hit), 

            // 3x MISS cases:
            //       ->  o                     o ->              | -> |
            // FallShort (t1>1,t2>1), Past (t1<0,t2<0), CompletelyInside(t1<0, t2>1)

            if (t1 >= 0 && t1 <= 1)
            {
                // t1 is the intersection, and it's closer than t2
                // (since t1 uses -b - discriminant)
                // Impale, Poke
                return true;
            }

            // here t1 didn't intersect so we are either started
            // inside the sphere or completely past it
            if (t2 >= 0 && t2 <= 1)
            {
                // ExitWound
                return true;
            }

            // no intn: FallShort, Past, CompletelyInside
            return false;
        }

        private class Target
        {
            public HealthComponent Health;
            public PositionComponent Position;
            public BodyComponent Body;
            public MovableComponent Movable;

            public class Creator : ObjectToNodeFilter<Target>.INodeCreator
            {
                public bool ConvertToNode(IComponentHolder gameObject, out Target result)
                {
                    result = new Target();
                    result.Health = gameObject.GetComponent<HealthComponent>();
                    result.Position = gameObject.GetComponent<PositionComponent>();
                    result.Body = gameObject.GetComponent<BodyComponent>();
                    result.Movable = gameObject.GetComponent<MovableComponent>();
                    return result.Health != null && result.Position != null && result.Body != null && result.Movable != null;
                }
            }
        }

        private class Projectile
        {
            public PositionComponent Position;
            public ProjectileComponent ProjectileComponent;
            public IComponentHolder GameObject;
            public MovableComponent Movable;

            public class Creator : ObjectToNodeFilter<Projectile>.INodeCreator
            {
                public bool ConvertToNode(IComponentHolder gameObject, out Projectile result)
                {
                    result = new Projectile();
                    result.ProjectileComponent = gameObject.GetComponent<ProjectileComponent>();
                    result.Position = gameObject.GetComponent<PositionComponent>();
                    result.GameObject = gameObject;
                    result.Movable = gameObject.GetComponent<MovableComponent>();
                    return result.ProjectileComponent != null && result.Position != null && result.Movable != null;
                }
            }
        }
    }
}
