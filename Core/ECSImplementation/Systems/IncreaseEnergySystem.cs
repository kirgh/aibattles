﻿using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    internal class IncreaseEnergySystem : AbstractSingleNodeSystem<DecreaseEnergySystemNode>
    {
        private readonly TimeSystem _time;

        public IncreaseEnergySystem(TimeSystem time)
        {
            _time = time;
        }

        protected override bool ConvertToNode(IComponentHolder gameObject, out DecreaseEnergySystemNode result)
        {
            var energy = gameObject.GetComponent<EnergyComponent>();
            result = energy != null ? new DecreaseEnergySystemNode(energy) : null;
            return result != null;
        }

        protected override void UpdateObject(DecreaseEnergySystemNode node)
        {
            node.Energy.Delta(_time.UpdateInterval * node.Energy.IncreaseSpeed);
        }
    }

    internal class DecreaseEnergySystemNode
    {
        public readonly EnergyComponent Energy;

        public DecreaseEnergySystemNode(EnergyComponent energy)
        {
            Energy = energy;
        }
    }
}
