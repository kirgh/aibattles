﻿using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// система, которая убивает всё живое имеющее координату, если оно не стоит на острове
    /// </summary>
    internal class KillByTerrainSystem : ISystem
    {

        private readonly ObjectToNodeFilter<UnitNode> _units;
        private readonly ObjectToNodeFilter<TerrainComponent> _islands;

        public KillByTerrainSystem()
        {
            _units = new ObjectToNodeFilter<UnitNode>(new UnitNodeCreator());
            _islands = new ObjectToNodeFilter<TerrainComponent>(new IslandNodeCreator());
        }

        public void AddObject(IComponentHolder gameObject)
        {
            _units.AddObject(gameObject);
            _islands.AddObject(gameObject);
        }

        public void RemoveObject(IComponentHolder gameObject)
        {
            _units.RemoveObject(gameObject);
            _islands.RemoveObject(gameObject);
        }

        public void Update()
        {
            for (var unitIndex = 0; unitIndex < _units.Output.Count; unitIndex++)
            {
                var unit = _units.Output[unitIndex];
                if (!unit.Health.IsAlive)
                {
                    continue;
                }
                bool unitIsOnIsland = false;
                for (var islandIndex = 0; islandIndex < _islands.Output.Count; islandIndex++)
                {
                    var island = _islands.Output[islandIndex];
                    if (unit.Position.Position.IsInRange(island.Position, island.Radius))
                    {
                        unitIsOnIsland = true;
                        break;
                    }
                }
                if (!unitIsOnIsland)
                {
                    unit.Health.Hit(unit.Health.Health);
                }
            }
        }

        private struct UnitNode
        {
            public PositionComponent Position;
            public HealthComponent Health;
        }

        private class UnitNodeCreator : ObjectToNodeFilter<UnitNode>.INodeCreator
        {
            public bool ConvertToNode(IComponentHolder gameObject, out UnitNode result)
            {
                result.Position = gameObject.GetComponent<PositionComponent>();
                result.Health = gameObject.GetComponent<HealthComponent>();
                return result.Position != null && result.Health != null;
            }
        }

        private class IslandNodeCreator : ObjectToNodeFilter<TerrainComponent>.INodeCreator
        {
            public bool ConvertToNode(IComponentHolder gameObject, out TerrainComponent result)
            {
                result = gameObject.GetComponent<TerrainComponent>();
                return result != null;
            }
        }
    }
}
