﻿using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    internal static class RemoveByLifetimeSystem
    {

        public static ISystem Create(IObjectsContainer container)
        {
            return new System(container);
        }

        private class System : AbstractSingleNodeSystem<Node>
        {
            private readonly IObjectsContainer _container;

            public System(IObjectsContainer container)
            {
                _container = container;
            }

            protected override bool ConvertToNode(IComponentHolder gameObject, out Node result)
            {
                result = new Node();
                result.Time = gameObject.GetComponent<TimeComponent>();
                result.RemoveByLifetime = gameObject.GetComponent<RemoveByLifetimeComponent>();
                result.Object = gameObject;
                return result.Time != null && result.RemoveByLifetime != null;
            }

            protected override void UpdateObject(Node node)
            {
                if (node.Time.TimeFromStart >= node.RemoveByLifetime.LifeTime)
                {
                    _container.RemoveObject(node.Object);
                }
            }
        }

        private class Node
        {
            public TimeComponent Time;
            public RemoveByLifetimeComponent RemoveByLifetime;
            public IComponentHolder Object;
        }
    }
}
