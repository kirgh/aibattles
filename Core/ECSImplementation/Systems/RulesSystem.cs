﻿using KM.Log;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// система, отвечающая за правила завершения игры
    /// TODO: должна расширяться в будущем
    /// </summary>
    internal class RulesSystem : AbstractNodeSystem<RulesSystemNode>
    {

        private readonly float _maxBattleTime;
        private readonly TimeComponent _worldTime;
        private readonly HashSet<int> _temporaryAliveTeamIds;

        public RulesSystem(TimeComponent worldTime, float maxBattleTime)
        {
            _worldTime = worldTime;
            _maxBattleTime = maxBattleTime;
            _temporaryAliveTeamIds = new HashSet<int>();
        }

        protected override bool ConvertToNode(IComponentHolder gameObject, out RulesSystemNode result)
        {
            result.Health = gameObject.GetComponent<HealthComponent>();
            result.Unit = gameObject.GetComponent<BattleUnitComponent>();
            return result.Health != null && result.Unit != null;
        }

        protected override void UpdateNodes(ReadOnlyCollection<RulesSystemNode> nodes)
        {
            Logger.TodoLowPriority("странно и глупо это всё получилось");
            _temporaryAliveTeamIds.Clear();
            for (var i = 0; i < nodes.Count; i++)
            {
                if (!nodes[i].Health.IsAlive)
                {
                    continue;
                }
                _temporaryAliveTeamIds.Add(nodes[i].Unit.Team.Id);
                if (_temporaryAliveTeamIds.Count > 1)
                {
                    break;
                }
            }
        }

        public bool IsFinished()
        {
            if (_worldTime.TimeFromStart >= _maxBattleTime)
            {
                return true;
            }
            return _temporaryAliveTeamIds.Count <= 1;
        }
    }

    internal struct RulesSystemNode
    {
        public BattleUnitComponent Unit;
        public HealthComponent Health;
    }
}
