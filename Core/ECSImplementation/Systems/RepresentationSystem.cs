﻿using KM.Log;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Z.KM.AIBattles.Common.Core.BattleRepresentation;
using Z.KM.AIBattles.Common.Geometry;
using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// "техническая" система для отображения мира наружу
    /// </summary>
    internal class RepresentationSystem : ISystem
    {

        private readonly SubSystem _subSystem;

        public RepresentationSystem(TimeComponent worldTime, TerrainComponent terrain)
        {
            _subSystem = new SubSystem(worldTime, terrain);
        }

        internal IBattleRepresentation GetRepresentation()
        {
            return _subSystem;
        }

        public void AddObject(IComponentHolder gameObject)
        {
            _subSystem.AddObject(gameObject);
        }

        public void RemoveObject(IComponentHolder gameObject)
        {
            _subSystem.RemoveObject(gameObject);
        }

        public void Update()
        {
            _subSystem.Update();
        }

        private class SubSystem : ISystem, IBattleRepresentation, ITerrain
        {
            private readonly UnitsSubSystem _units;
            private readonly ProjectilesSubSystem _projectiles;
            private readonly TimeComponent _worldTime;
            private readonly TerrainComponent _terrain;

            public SubSystem(TimeComponent worldTime, TerrainComponent terrain)
            {
                _units = new UnitsSubSystem();
                _projectiles = new ProjectilesSubSystem();
                _worldTime = worldTime;
                _terrain = terrain;
            }


            public float SecondsFromStart
            {
                get
                {
                    return _worldTime.TimeFromStart;
                }
            }

            Vector2D ITerrain.Position
            {
                get
                {
                    return _terrain.Position;
                }
            }

            float ITerrain.Radius
            {
                get
                {
                    return _terrain.Radius;
                }
            }

            public float MaxBattleTime
            {
                get
                {
                    return _terrain.EndShrinkTime;
                }
            }

            public ReadOnlyCollection<IBattleUnitRepresentation> GetUnits()
            {
                return _units.GetUnits();
            }

            public ReadOnlyCollection<IBattleProjectileRepresentation> GetProjectiles()
            {
                return _projectiles.GetProjectiles();
            }

            public void AddObject(IComponentHolder gameObject)
            {
                _units.AddObject(gameObject);
                _projectiles.AddObject(gameObject);
            }

            public void RemoveObject(IComponentHolder gameObject)
            {
                _units.RemoveObject(gameObject);
                _projectiles.RemoveObject(gameObject);
            }

            public void Update()
            {
                _units.Update();
                _projectiles.Update();
            }

            public ITerrain GetTerrain()
            {
                return this;
            }
        }

        private class UnitsSubSystem : AbstractNodeSystem<UnitRepresentation>
        {
            private ReadOnlyCollection<IBattleUnitRepresentation> _units;

            public ReadOnlyCollection<IBattleUnitRepresentation> GetUnits()
            {
                return _units;
            }

            protected override bool ConvertToNode(IComponentHolder gameObject, out UnitRepresentation result)
            {
                result.Unit = gameObject.GetComponent<BattleUnitComponent>();
                result.HealthComponent = gameObject.GetComponent<HealthComponent>();
                result.PositionComponent = gameObject.GetComponent<PositionComponent>();
                result.Movable = gameObject.GetComponent<MovableComponent>();
                result.Body = gameObject.GetComponent<BodyComponent>();
                return result.Unit != null
                    && result.HealthComponent != null
                    && result.PositionComponent != null
                    && result.Body != null
                    && result.Movable != null;
            }

            protected override void UpdateNodes(ReadOnlyCollection<UnitRepresentation> nodes)
            {
                Logger.TodoLowPriority("вот именно тут раскрывается дыра");
                _units = nodes.Cast<IBattleUnitRepresentation>().ToList().AsReadOnly();
            }
        }

        private struct UnitRepresentation : IBattleUnitRepresentation
        {

            public BattleUnitComponent Unit;
            public HealthComponent HealthComponent;
            public PositionComponent PositionComponent;
            public BodyComponent Body;
            public MovableComponent Movable;

            public IBattleTeamRepresentation Team
            {
                get
                {
                    return Unit.Team;
                }
            }

            public int Health
            {
                get
                {
                    return HealthComponent.Health;
                }
            }

            public int MaxHealth
            {
                get
                {
                    return HealthComponent.MaxHealth;
                }
            }

            public Vector2D Position
            {
                get
                {
                    return PositionComponent.Position;
                }
            }

            public float Radius
            {
                get
                {
                    return Body.Radius;
                }
            }

            public Vector2D Speed
            {
                get
                {
                    return Movable.Speed;
                }
            }

            public StringBuilder GetDebugString()
            {
                return new StringBuilder(string.Format("Unit Team = {0}, Health = {1}, Position = {2}", Team.Id, Health, Position));
            }
        }


        private class ProjectilesSubSystem : AbstractNodeSystem<ProjectileRepresentation>
        {

            private ReadOnlyCollection<IBattleProjectileRepresentation> _projectiles;

            internal ReadOnlyCollection<IBattleProjectileRepresentation> GetProjectiles()
            {
                return _projectiles;
            }

            protected override bool ConvertToNode(IComponentHolder gameObject, out ProjectileRepresentation result)
            {
                result = new ProjectileRepresentation();
                result.Projectile = gameObject.GetComponent<ProjectileComponent>();
                result.Movable = gameObject.GetComponent<MovableComponent>();
                result.PositionComponent = gameObject.GetComponent<PositionComponent>();
                return result.Movable != null && result.Projectile != null && result.PositionComponent != null;
            }

            protected override void UpdateNodes(ReadOnlyCollection<ProjectileRepresentation> nodes)
            {
                Logger.TodoLowPriority("вот именно тут раскрывается дыра");
                _projectiles = nodes.Cast<IBattleProjectileRepresentation>().ToList().AsReadOnly();
            }

        }

        private class ProjectileRepresentation : IBattleProjectileRepresentation
        {
            public PositionComponent PositionComponent;
            public MovableComponent Movable;
            public ProjectileComponent Projectile;

            public Vector2D Position
            {
                get
                {
                    return PositionComponent.Position;
                }
            }

            public Vector2D Speed
            {
                get
                {
                    return Movable.Speed;
                }
            }
        }
    }
}
