﻿using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// система, которая постепенно сужает острова
    /// </summary>
    internal class ShrinkTerrainSystem : AbstractSingleNodeSystem<ShrinkTerrainSystemNode>
    {
        protected override bool ConvertToNode(IComponentHolder gameObject, out ShrinkTerrainSystemNode result)
        {
            result.Terrain = gameObject.GetComponent<TerrainComponent>();
            result.Time = gameObject.GetComponent<TimeComponent>();
            return result.Terrain != null && result.Time != null;
        }

        protected override void UpdateObject(ShrinkTerrainSystemNode node)
        {
            var endShrinkTime = node.Terrain.EndShrinkTime;
            var startShrinkTime = node.Terrain.StartShrinkTime;
            if (node.Time.TimeFromStart < startShrinkTime)
            {
                node.Terrain.Radius = node.Terrain.StartRadius;
                return;
            }
            if (node.Time.TimeFromStart >= endShrinkTime)
            {
                node.Terrain.Radius = 0;
                return;
            }
            var shrinkSpeed = node.Terrain.StartRadius / (endShrinkTime - startShrinkTime);
            node.Terrain.Radius = node.Terrain.StartRadius - shrinkSpeed * (node.Time.TimeFromStart - startShrinkTime);
        }
    }

    internal struct ShrinkTerrainSystemNode
    {
        public TerrainComponent Terrain;
        public TimeComponent Time;
    }
}
