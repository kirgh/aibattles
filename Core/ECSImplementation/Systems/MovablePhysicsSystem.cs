﻿using Z.KM.AIBattles.Common.Geometry;
using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// кривая и полностью на переработку система,
    /// отвечающая за физику объектов, желающих двигаться в физическом мире.
    /// </summary>
    internal class MovablePhysicsSystem : ISystem
    {

        private readonly SubSystem _subSystem;

        public MovablePhysicsSystem(float updateInterval)
        {
            _subSystem = new SubSystem(updateInterval);
        }

        public void AddObject(IComponentHolder gameObject)
        {
            _subSystem.AddObject(gameObject);
        }

        public void RemoveObject(IComponentHolder gameObject)
        {
            _subSystem.RemoveObject(gameObject);
        }

        public void Update()
        {
            _subSystem.Update();
        }

        private class SubSystem : AbstractSingleNodeSystem<SubSystemNode>
        {
            private readonly float _updateInterval;

            public SubSystem(float updateInterval)
            {
                _updateInterval = updateInterval;
            }

            protected override bool ConvertToNode(IComponentHolder gameObject, out SubSystemNode result)
            {
                result.Movable = gameObject.GetComponent<MovableComponent>();
                result.SelfMovable = gameObject.GetComponent<SelfMovableComponent>();
                return result.Movable != null && result.SelfMovable != null;
            }

            protected override void UpdateObject(SubSystemNode node)
            {
                var speed = node.Movable.Speed;

                if (!node.SelfMovable.DesiredSpeed.Equals(Vector2D.Zero))
                {
                    var delta = node.SelfMovable.DesiredSpeed - speed;
                    var modifiedDelta = delta.LimitMagnitude(_updateInterval * node.SelfMovable.Acceleration);
                    speed = speed + modifiedDelta;
                }
                else
                {
                    var speedValue = speed.GetLength();
                    if (speedValue > 0)
                    {
                        var newSpeedValue = speedValue - node.SelfMovable.Deceleration * _updateInterval;
                        speedValue = newSpeedValue > 0 ? newSpeedValue : 0;
                        speed = speed.Normalize(speedValue);
                    }
                }

                node.Movable.Speed = speed;
            }
        }

        private struct SubSystemNode
        {
            public MovableComponent Movable;
            public SelfMovableComponent SelfMovable;
        }
    }
}
