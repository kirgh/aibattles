﻿using Z.KM.AIBattles.Common.Geometry;
using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.ECS;
using Z.KM.ECS.BaseSystems;

namespace Z.KM.AIBattles.Core.ECSImplementation.Systems
{
    /// <summary>
    /// примитивная система "расталкивания" объектов
    /// в большом проекте такой нет, но в нашем случае это хороший чит
    /// </summary>
    internal class NormalizePositionsSystem : AbstractNodeToNodeSystem<NormalizePositionsSystemNode>
    {
        protected override bool ConvertToNode(IComponentHolder gameObject, out NormalizePositionsSystemNode result)
        {
            result = new NormalizePositionsSystemNode();
            result.Health = gameObject.GetComponent<HealthComponent>();
            result.Movable = gameObject.GetComponent<PositionComponent>();
            result.Body = gameObject.GetComponent<BodyComponent>();
            return result.Health != null && result.Movable != null && result.Body != null;
        }

        protected override void UpdateObjects(NormalizePositionsSystemNode a, NormalizePositionsSystemNode b)
        {
            if (!a.Health.IsAlive || !b.Health.IsAlive)
            {
                return;
            }


            var delta = b.Movable.Position - a.Movable.Position;
            var minDistance = a.Body.Radius + b.Body.Radius;
            var distanceBetween = delta.GetLength();
            if (distanceBetween >= minDistance)
            {
                return;
            }
            if (distanceBetween == 0)
            {
                delta = new Vector2D(1, 0);
            }
            delta = delta.Normalize() * (minDistance - distanceBetween);

            a.Movable.Position = a.Movable.Position - delta / 2;
            b.Movable.Position = b.Movable.Position + delta / 2;
        }
    }

    internal class NormalizePositionsSystemNode
    {
        public HealthComponent Health;
        public PositionComponent Movable;
        public BodyComponent Body;
    }
}
