﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Z.KM.AIBattles.Core.Utils
{
    /// <summary>
    /// словарь TKeyType -> TObjectType : TObjectInterface,
    /// который на выходе даёт ReadOnlyCollection[TObjectInterface]
    /// </summary>
    internal class DictionaryToReadOnlyCollection<TKeyType, TObjectType, TObjectInterface> where TObjectType : TObjectInterface, new()
    {
        private readonly HashSet<TKeyType> _containingObjects;
        private readonly Dictionary<TKeyType, TObjectType> _holderToRecord;
        private readonly List<TObjectInterface> _output;
        public ReadOnlyCollection<TObjectInterface> Output { private set; get; }

        public DictionaryToReadOnlyCollection()
        {
            _holderToRecord = new Dictionary<TKeyType, TObjectType>();
            _output = new List<TObjectInterface>();
            _containingObjects = new HashSet<TKeyType>();
            Output = _output.AsReadOnly();
        }

        public void Remove(TKeyType id)
        {
            if (!_containingObjects.Contains(id))
            {
                return;
            }
            TObjectType existing;
            _containingObjects.Remove(id);
            _holderToRecord.TryGetValue(id, out existing);
            _holderToRecord.Remove(id);
            _output.Remove(existing);
        }

        public TObjectType GetOrCreate(TKeyType id)
        {
            if (_containingObjects.Contains(id))
            {
                return _holderToRecord[id];
            }
            var result = new TObjectType();
            _holderToRecord[id] = result;
            _containingObjects.Add(id);
            _output.Add(result);
            return result;
        }
    }
}
