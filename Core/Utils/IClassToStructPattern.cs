﻿namespace Z.KM.AIBattles.Core.Utils
{
    interface IClassToStructPattern<TStruct> where TStruct : struct
    {
        TStruct GetData();
    }
}
