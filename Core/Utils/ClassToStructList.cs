﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Z.KM.AIBattles.Core.Utils
{
    internal class ClassToStructList<TClass, TStruct>
        where TStruct : struct
        where TClass : IClassToStructPattern<TStruct>
    {

        public readonly List<TClass> Raw;

        private List<TStruct> _resultList;
        private ReadOnlyCollection<TStruct> _resultReadonlyCollection;

        private ClassToStructList(List<TClass> myOwnNewList)
        {
            Raw = myOwnNewList;
            _resultList = new List<TStruct>(Raw.Count);
            _resultReadonlyCollection = _resultList.AsReadOnly();
            Refresh();
        }

        public int Count { get { return Raw.Count; } }

        public static ClassToStructList<TClass, TStruct> CreateWithCopy(ReadOnlyCollection<TClass> collection)
        {
            return new ClassToStructList<TClass, TStruct>(collection.ToList());
        }

        public static ClassToStructList<TClass, TStruct> CreateEmpty()
        {
            return new ClassToStructList<TClass, TStruct>(new List<TClass>());
        }

        public ReadOnlyCollection<TStruct> GetData()
        {
            Refresh();
            return _resultReadonlyCollection;
        }

        private void Refresh()
        {
            var count = Raw.Count;
            if (_resultList.Count != count)
            {
                _resultList = new List<TStruct>(count);
                for(var i = 0; i < count; i++)
                {
                    _resultList.Add(Raw[i].GetData());
                }
                _resultReadonlyCollection = _resultList.AsReadOnly();
                return;
            }

            for (var i = 0; i < count; i++)
            {
                _resultList[i] = Raw[i].GetData();
            }
        }
    }
}
