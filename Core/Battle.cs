﻿using KM.Log;
using System;
using System.Collections.Generic;
using Z.KM.AIBattles.Common.AI;
using Z.KM.AIBattles.Common.Core;
using Z.KM.AIBattles.Common.Core.BattleRepresentation;
using Z.KM.AIBattles.Common.Geometry;
using Z.KM.ECS;
using Z.KM.AIBattles.Core.ECSImplementation.Components;
using Z.KM.AIBattles.Core.ECSImplementation.Systems;

namespace Z.KM.AIBattles.Core
{
    /// <summary>
    /// главный класс для проведения боя
    /// </summary>
    internal class Battle : IBattle, IObjectsContainer
    {

        private readonly ECSCore _ecsCore;
        private readonly RepresentationSystem _representationSystem;
        private readonly RulesSystem _rulesSystem;

        public Battle(CreateBattleSettings settings)
        {
            Logger.TodoLowPriority("раздача TimeSystem, TerrainComponent и WorldTime - грех");

            _ecsCore = new ECSCore();

            var worldTime = new TimeComponent();
            var timeSystem = new TimeSystem(settings.TickInterval);

            var terrainGameObject = new GameObject();
            var terrainComponent = new TerrainComponent(settings.TerrainPosition, settings.TerrainRadius, settings.StartShrinkTerrainTime, settings.MaxBattleTime);
            terrainGameObject.AddComponent(terrainComponent);
            terrainGameObject.AddComponent(new TimeComponent());
            AddObject(terrainGameObject);

            _ecsCore.AddSystem(timeSystem);
            _ecsCore.AddSystem(new ResetPhysicsSystem());
            _ecsCore.AddSystem(new IncreaseEnergySystem(timeSystem));
            _ecsCore.AddSystem(new PerceptionSystem(terrainComponent));
            _ecsCore.AddSystem(new AISystem(worldTime, terrainComponent));
            _ecsCore.AddSystem(new MovablePhysicsSystem(settings.TickInterval));
            _ecsCore.AddSystem(new MovingSystem(timeSystem)); //должна быть перед CastingSystem иначе melee прожектайлы не попадут
            _ecsCore.AddSystem(new CastingSystem(this));
            _ecsCore.AddSystem(new DamageByProjectilesSystem(this, settings.TickInterval));
            _ecsCore.AddSystem(new NormalizePositionsSystem());
            _ecsCore.AddSystem(new ShrinkTerrainSystem());
            _ecsCore.AddSystem(new KillByTerrainSystem());
            _ecsCore.AddSystem(RemoveByLifetimeSystem.Create(this));
            _ecsCore.AddSystem(_representationSystem = new RepresentationSystem(worldTime, terrainComponent));
            _ecsCore.AddSystem(_rulesSystem = new RulesSystem(worldTime, settings.MaxBattleTime));

            var worldTimeGameObject = new GameObject();
            worldTimeGameObject.AddComponent(worldTime);
            AddObject(worldTimeGameObject);

            var teams = new List<BattleTeam>();
            var random = new Random(settings.RandomSeed);
            foreach (var teamSettings in settings.Teams)
            {
                var team = new BattleTeam(teamSettings.Id, teamSettings.AI);
                InitAiFactoryData initAiData;
                initAiData.Random = random;
                initAiData.TeamId = team.Id;
                teamSettings.AI.Init(initAiData);
                teams.Add(team);
                for (var unitIndex = 0; unitIndex < teamSettings.Units.Count; unitIndex++)
                {
                    AddObject(CreateUnit(settings, teamSettings.Units[unitIndex], team, unitIndex));
                }
            }

            Logger.TodoLowPriority("этого не должно быть!");
            _rulesSystem.Update();
            _representationSystem.Update();
        }

        private static GameObject CreateUnit(CreateBattleSettings settings, CreateBattleSettings.Unit unit, BattleTeam team, int unitId)
        {
            var gameObject = new GameObject();
            gameObject.AddComponent(new PositionComponent(unit.Position));
            gameObject.AddComponent(new MovableComponent(Vector2D.Zero));
            gameObject.AddComponent(new HealthComponent(unit.MaxHealth));
            gameObject.AddComponent(new BodyComponent(settings.UnitRadius));
            gameObject.AddComponent(new CasterComponent(settings.ProjectileDamage, settings.ProjectileSpeed, settings.Energy.AttackCost, settings.ProjectileLifeTime, settings.ProjectilePushTargetCoefficient));
            PerceptionSystem.InitUnitComponents(gameObject);
            CreateUnitAiData createUnitData;
            createUnitData.UniqueIdInTeam = unitId;
            createUnitData.ProjectileSpeed = settings.ProjectileSpeed;
            createUnitData.UnitSpeed = settings.UnitsMaxSpeed;
            gameObject.AddComponent(new AIComponent(team.AI.CreateUnitAi(createUnitData)));
            gameObject.AddComponent(new BattleUnitComponent(team, unitId));
            gameObject.AddComponent(new EnergyComponent(settings.Energy));
            gameObject.AddComponent(new SelfMovableComponent(settings.UnitDeceleration, settings.UnitAcceleration, settings.UnitsMaxSpeed));
            return gameObject;
        }

        public void AddObject(IComponentHolder gameObject)
        {
            _ecsCore.AddObject(gameObject);
        }

        public void RemoveObject(IComponentHolder gameObject)
        {
            _ecsCore.RemoveObject(gameObject);
        }

        public IBattleRepresentation GetRepresentation()
        {
            return _representationSystem.GetRepresentation();
        }

        public bool IsFinished()
        {
            return _rulesSystem.IsFinished();
        }

        public void NextStep()
        {
            _ecsCore.Update();
        }

        private class BattleTeam : IBattleTeamRepresentation
        {
            public readonly IAiFactory AI;
            public int Id { get; private set; }

            public BattleTeam(int id, IAiFactory ai)
            {
                Id = id;
                AI = ai;
            }

            public bool IsFriendly(IBattleTeamRepresentation otherTeam)
            {
                return Id == otherTeam.Id;
            }
        }
    }
}