﻿
using System.Collections.ObjectModel;

namespace Z.KM.AIBattlesCore.Implementation
{

    interface IBatleWorld
    {
        BattleWorldData Data { get; }
    }

    class BattleWorldImplementation : IBatleWorld
    {
        private readonly ReadOnlyCollection<BattleUnit> _publicUnits;

        public BattleWorldData Data
        {
            get
            {
                BattleWorldData result;
                result.Units = _publicUnits;
                return result;
            }
        }

        public ReadOnlyCollection<BattleUnit> Units
        {
            get
            {
                return _publicUnits;
            }
        }
    }
}
