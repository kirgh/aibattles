﻿using Z.KM.ECS;

namespace Z.KM.AIBattles.Core
{
    /// <summary>
    /// что-то, что позволяет добавлять (и может быть удалять) объекты.
    /// обычно это мир, и его не отдают кому попало.
    /// </summary>
    public interface IObjectsContainer
    {
        void AddObject(IComponentHolder gameObject);
        void RemoveObject(IComponentHolder gameObject);
    }
}
